<?php namespace Asistance;

use Illuminate\Database\Eloquent\Model;

class ModelTipoMarca extends Model {

	protected $table='Marcado';

	//Debido a que la DB ya está creada, no fue creada con migraciones y por ello, 
	//no se incluirán los timestamps
	public $timestamps = false;

    protected $fillable = ['id_marcado', 'Tipo_marca'];
    
	protected $primaryKey = 'id_marcado';
	
	//Query Scope para la búsqueda de celulares
	public function scopeTipo_marca($query, $Tipo_marca)
    {
        if($Tipo_marca)       
        {
            return $query->where('Tipo_marca', 'LIKE', "%$Tipo_marca%");
        } 
    }

}
