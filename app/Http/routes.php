<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//LOGIN
Route::resource('login', 'LoginController');
Route::get('/', 'LoginController@index');
//Logout seasson...
Route::get('logout', 'LoginController@logout');

//MAIN
Route::get('main', 'MainController@index');
Route::get('main/usuarios', 'MainController@usuarios');
Route::get('main/zonageo', 'MainController@zonageo');
Route::get('main/ausentismo', 'MainController@ausentismo');
Route::get('main/horarios', 'MainController@horarios');
Route::get('main/marcado', 'MainController@marcado');
//Recuperación PW
Route::get('main/cambiapw', 'MainController@cambiapw');
//Cambiar la contraseña
Route::post('main/pwreset', 'MainController@pwreset');

//USUARIOS
Route::resource('main/usuarios/administrar', 'UsuariosController');
Route::get('main/usuarios/nuevo','UsuariosController@nuevo');
Route::get('main/usuarios/export','UsuariosController@exportaUsuario');
Route::post('main/usuarios/administrar/borra','UsuariosController@delete');

//CELULARES
Route::resource('main/usuarios/administrarcel', 'CelularController');
Route::post('main/usuarios/administrarcel/borra','CelularController@delete');
Route::get('main/usuarios/celular','CelularController@celular');
Route::get('main/usuarios/exportcel','CelularController@exportaCelular');

//ZONAS GEOGRÁFICAS
Route::resource('main/zonageo/administrar', 'ZonaGeoController');
Route::resource('main/zonageo/asigadministrar', 'AsigZonaGeoController');
Route::post('main/zonageo/administrar/borra','ZonaGeoController@delete');
Route::post('main/zonageo/asigadministrar/borra','AsigZonaGeoController@delete');
Route::get('main/zonageo/nuevo','ZonaGeoController@nuevo');
Route::get('main/zonageo/asignuevo','AsigZonaGeoController@nuevo');
Route::get('main/zonageo/export','ZonaGeoController@exportaZonaGeo');
Route::get('main/zonageo/asigexport','AsigZonaGeoController@exportaZonaGeo');

//AUSENTISMO
Route::resource('main/ausentismo/administrar', 'AusentismoController');
Route::resource('main/ausentismo/asigadministrar', 'AsigAusentismoController');
Route::post('main/ausentismo/administrar/borra','AusentismoController@delete');
Route::post('main/ausentismo/asigadministrar/borra','AsigAusentismoController@delete');
Route::get('main/ausentismo/nuevo','AusentismoController@nuevo');
Route::get('main/ausentismo/asignuevo','AsigAusentismoController@nuevo');
Route::get('main/ausentismo/export','AusentismoController@exportaAusentismo');
Route::get('main/ausentismo/asigexport','AsigAusentismoController@exportaAusentismo');

//HORARIOS
Route::resource('main/horarios/administrar', 'HorariosController');
Route::resource('main/horarios/asigadministrar', 'AsigHorariosController');
Route::post('main/horarios/administrar/borra','HorariosController@delete');
Route::post('main/horarios/asigadministrar/borra','AsigHorariosController@delete');
Route::get('main/horarios/nuevo','HorariosController@nuevo');
Route::get('main/horarios/asignuevo','AsigHorariosController@nuevo');
Route::get('main/horarios/export','HorariosController@exportaHorario');
Route::get('main/horarios/asigexport','AsigHorariosController@exportaHorario');

//MARCAS Y CONSULTAS DE MARCAS
Route::resource('main/marcado/administrar', 'MarcadoController');
Route::get('main/marcado/marcau','MarcadoController@marcau');
Route::post('main/marcado/administrar/borra','MarcadoController@delete');
Route::get('main/marcado/reporte','MarcadoController1@reporte');
Route::get('main/marcado/export','MarcadoController@exportaMarcas');
