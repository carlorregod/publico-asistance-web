<?php namespace Asistance\Http\Requests;

use Asistance\Http\Requests\Request;

class AsigHorariosRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'Fecha_inicio' => 'required',
			'Fecha_fin' => 'required',
		];
	}

}
