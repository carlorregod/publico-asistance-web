<?php namespace Asistance\Http\Requests;

use Asistance\Http\Requests\Request;

class UsuarioRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'RUT' => 'required|max:10',
			'Apellido_P' => 'required|max:15',
			'Apellido_M' => 'required|max:15',
			'Nombres' => 'required|max:25',
			'Domicilio' => 'required:max:50',
			'Telefono' => 'required',
			'Fecha_nacimiento' => 'required',
			'Fecha_ingreso' => 'required',
			'Rol' => 'required',
			'password' => 'required|max:30',
			'email' => 'required|max:40',
		];
	}

}
