<?php namespace Asistance\Http\Requests;

use Asistance\Http\Requests\Request;

class GeolocalizacionRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'Alias_loc_geo' => 'required|max:24', 
			'coorX1' => 'required', 
			'coorY1'=> 'required', 
			'coorX2'=> 'required', 
			'coorY2'=> 'required',
		];
	}

}
