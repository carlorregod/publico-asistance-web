<?php namespace Asistance\Http\Requests;

use Asistance\Http\Requests\Request;

class CelularRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'Numero_celular' => 'required|max:11', 
			'Compania' => 'max:18',
			'IMEI' => 'max:18',
		];
	}

}
