<?php namespace Asistance\Http\Requests;

use Asistance\Http\Requests\Request;

class MarcadoRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'RUT' => 'required',
			'id_marcado' => 'required',
			'id_celular' => 'required',
			'Fecha_marcado' => 'required',
			'Hora_marca' => 'required',
			'coordX_GPS' => 'required',
			'coordY_GPS' => 'required',
		];
	}

}
