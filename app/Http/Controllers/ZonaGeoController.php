<?php namespace Asistance\Http\Controllers;

use Asistance\Http\Requests;
use Asistance\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Asistance\Http\Requests\GeolocalizacionRequest;
//Enlace a la ruta del modelo:
use Asistance\ModelGeolocalizacion;
//Para llamar a atributos reservados especiales:
use Illuminate\Support\Facades\Session; 
use Illuminate\Support\Facades\Redirect;
use DB;
use Maatwebsite\Excel\Facades\Excel;

class ZonaGeoController extends Controller {

	/*Protegiendo las rutas asociadas a este controlador */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$zonageos= ModelGeolocalizacion::orderBy('Alias_loc_geo', 'DESC')
			->paginate(15);
		return view('sistema.zonageo.administra',compact('zonageos'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(GeolocalizacionRequest $request)
	{
		//Primera lógica de guardado: Buscar si el alias de zonageográfica existe o no en el softdelete
		$logica=false;
		$comparadors=ModelGeolocalizacion::onlyTrashed()->get(); //sólo valores softdeleteados.
		foreach($comparadors as $comparador)
		{
			if($comparador->Alias_loc_geo == $request->Alias_loc_geo) //comparación)
			{
				$logica= true;
				break;
			}
		}
		//Segunda lógica: buscar el alias de loc geo en los valores normales no softdeleteados
		unset($comparadors); //limpieza de variable
		$comparadors=ModelGeolocalizacion::All();
		$logica2=false;
		foreach($comparadors as $comparador)
		{
			if($comparador->Alias_loc_geo == $request->Alias_loc_geo) //comparación)
			{
				$logica2= true;
				break;
			}
		}
		//Se compara logica, si es true significa que el alias ya fue empleado. hay que comparar la negación
		if($logica)
		{
			//Simplemente, se vuelve a validar el uso de ese usuario cuando hay registro previo y si hay registro de borrado en deleted_at
			ModelGeolocalizacion::withTrashed()-> where('Alias_loc_geo', '=', $request->Alias_loc_geo ) ->restore();
			Session::flash('message', 'No se genera nueva zona geográfica, pero se habilita zona creada anteriormente.');
			return Redirect::to('main\zonageo\nuevo');
		}
		//Se compara logica, si es false significa que el alias ya fue empleado.
		elseif(!$logica2)
		{
			//Verificación de coherencia de los puntos seleccionados en el mapa:
			//La lógica del eje X es longitud. Lógica eje Y es latitud.
			/*
									(dot)X2Y2

					(dot)X1Y1
			*/
		if ($request->coorX1 > $request->coorX2)
		//Si los puntos no corresponden se hará un intercambio de variables.
		//Recordar la condición normal correcta: $request->coorX1 < $request->coorX2) || ($request->coorY2 > $request->coorY1
		{
			//Intercambio eje X
			$cambio=$request->coorX1;
			$request->coorX1=$request->coorX2;
			$request->coorX2=$cambio;
		}
		if($request->coorY2 < $request->coorY1)
		{
			//Intercambio eje Y
			$cambio=$request->coorY1;
			$request->coorY1=$request->coorY2;
			$request->coorY2=$cambio;
		}
				ModelGeolocalizacion::create
				([
					'Alias_loc_geo' =>$request['Alias_loc_geo'],
					'coorX1' => $request['coorX1'],
					'coorY1' => $request['coorY1'],
					'coorX2' => $request['coorX2'],
					'coorY2' => $request['coorY2'],
				]);
					Session::flash('message', 'Nueva zona geográfica generada exitosamente');
					return Redirect::to('main/zonageo/nuevo');
		}
		else
		{
			Session::flash('message', 'Nueva zona geográfica no generada. Ya existe una zona con el mismo nombre.');
			return Redirect::to('main/zonageo/nuevo');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, Request $request)
	{
		try
		{
			$Alias_loc_geo=$request->get('Alias_loc_geo');
			//Asignación a variable consulta de todos los elementos de la tabla de la relación "ZonaGeo"
			$zonageos= ModelGeolocalizacion::orderBy('Alias_loc_geo', 'DESC')
				->Alias_loc_geo($Alias_loc_geo)
				->paginate(15);

			//Retorno de la vista asociasa a edición
			return view('sistema.zonageo.administra',compact('zonageos'));
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible filtrar. Dato en uso actualmente.');
			return Redirect::to('main\zonageo\administrar');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$zonageo=ModelGeolocalizacion::find($id);
		return view('sistema.zonageo.edit',['zonageo'=>$zonageo]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, GeolocalizacionRequest $request)
	{
		$zonageo=ModelGeolocalizacion::find($id);
		//Si los puntos no corresponden se hará un intercambio de variables.
		//Recordar la condición normal correcta: $request->coorX1 < $request->coorX2) || ($request->coorY2 > $request->coorY1
		if ($request->coorX1 > $request->coorX2 && $request->coorY2 < $request->coorY1)
		{
			$zonageo->fill([
				'coorX1' => $request['coorX2'],
				'coorY1' => $request['coorY2'],
				'coorX2' => $request['coorX1'],
				'coorY2' => $request['coorY1'],
			]);
			$zonageo->save();
		}
		elseif ($request->coorX1 > $request->coorX2)
		{
			$zonageo->fill([
				'coorX1' => $request['coorX2'],
				'coorY1' => $request['coorY1'],
				'coorX2' => $request['coorX1'],
				'coorY2' => $request['coorY2'],
			]);
			$zonageo->save();
		}
		elseif($request->coorY2 < $request->coorY1)
		{
			$zonageo->fill([
				'coorX1' => $request['coorX1'],
				'coorY1' => $request['coorY2'],
				'coorX2' => $request['coorX2'],
				'coorY2' => $request['coorY1'],
			]);
			$zonageo->save();
		}
		else
		{
			$zonageo->fill([
				'coorX1' => $request['coorX1'],
				'coorY1' => $request['coorY1'],
				'coorX2' => $request['coorX2'],
				'coorY2' => $request['coorY2'],
			]);
			$zonageo->save();
		}		
			Session::flash('message', 'Edición realizada exitosamente');
			return Redirect::to('main\zonageo\administrar');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try
		{
			//Antes de eliminar una zona geográfica, hay que asegurarse que el número no esté asignado a un trabajador
			$asignacion = DB::table('Asignacion_loc_geo')->get();
			$logica = true;
			foreach($asignacion as $asigna)
			{
				if($asigna->id_loc_geo == $id)
				{
					$logica = false;
					break;
				}
			}
			//$logica será verdadero en el caso que la zona no esté previamente asignado.
			if($logica)
			{
				$zonageo=ModelGeolocalizacion::find($id);
				$zonageo->delete();
				Session::flash('message', 'Eliminación realizada exitosamente');
				return Redirect::to('main\zonageo\administrar');
			}
			else
			{
				Session::flash('message', 'Zona geográfica asignada a uno o varios usuarios. Desasignar antes de borrar.');
				return Redirect::to('main\zonageo\administrar');
			}			
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible borrar. Dato en uso actualmente.');
			return Redirect::to('main\zonageo\administrar');
		}
	}

	public function nuevo()
    {
        return view('sistema.zonageo.nuevo');
	}

	public function delete(Request $request)
	{
		try
		{
			//Se recolecta en un arreglo, todas las ID a borrar
			$delets=$request->delete;
			//Evitar errores con un if... en caso que no existan checkboxs seleccionados es decir, no hay $delets
			if($delets)
			{
				//Un primer foreach doble, para revisar que los delets elegidos no estén en uso.
				$asignacion = DB::table('Asignacion_loc_geo')->get();
				$logica = true;	
				foreach($delets as $delet)
				{
					foreach($asignacion as $asigna)
					{
						if($asigna->id_loc_geo == $delet)
						{
							$logica = false;
							break;
						}
					}
				}
				if($logica)
				{
					//Se recorre el arreglo $delets... para hacer el borrado
					foreach($delets as $delet)
					{
						$zonageo=ModelGeolocalizacion::find($delet);
						$zonageo->delete();
					}
					Session::flash('message', 'Eliminación masiva realizada exitosamente');
					return Redirect::to('main\zonageo\administrar');
				}
				else
				{
					Session::flash('message', 'Una o varias de las zonas geográficas seleccionados están asignados a uno o varios usuarios. Desasignar antes del borrado.');
					return Redirect::to('main\zonageo\administrar');
				}				
			}
			else
			{
				Session::flash('message', 'No se han efectuado selecciones.');
				return Redirect::to('main\zonageo\administrar');
			}			
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible borrar. Dato en uso actualmente.');
			return Redirect::to('main\zonageo\administrar');
		}
	}

	public function exportaZonaGeo()
	{
		Excel::create('Zona_Geografica(Listado)', function($excel){
			$excel->sheet('Zonageo', function($sheet){
				$data = ModelGeolocalizacion::where('deleted_at', '=', null)
					->select('Alias_loc_geo', 'coorX1', 'coorY1', 'coorX2', 'coorY2')
					->get(); 
				$sheet->fromArray($data);
			});
		})->download('xls');
		return back();
	}
	
}
