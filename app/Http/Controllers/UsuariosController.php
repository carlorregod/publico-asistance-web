<?php namespace Asistance\Http\Controllers;

use Asistance\Http\Requests;
use Asistance\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Asistance\Http\Requests\UsuarioRequest;

//Enlace a la ruta del modelo:
use Asistance\ModelUsuario;
//Enlace a la ruta de los celulares
use Asistance\ModelCelular;

//Para llamar a atributos reservados especiales:
use Illuminate\Support\Facades\Session; 
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use DB;
use Maatwebsite\Excel\Facades\Excel;

class UsuariosController extends Controller {

	/*Protegiendo las rutas asociadas a este controlador */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$usuarios = DB::table('Usuario')
			->where('Usuario.deleted_at','=',null)
			->join('Celular', 'Celular.id_celular', '=', 'Usuario.Telefono')
			->orderBy('RUT', 'DESC')
			->paginate(10);
		return view('sistema.usuarios.administra',compact('usuarios'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(UsuarioRequest $request)
	{
		//Primera lógica de guardado: Buscar si el alias de usuario existe o no en el softdelete
		$logica=false;
		$comparadors=ModelUsuario::onlyTrashed()->get(); //sólo valores softdeleteados.
		foreach($comparadors as $comparador)
		{
			if($comparador->RUT == $request->RUT) //comparación)
			{
				$logica= true;
				break;
			}
		}
		//Segunda lógica: buscar el RUT en los valores normales no softdeleteados
		unset($comparadors); //limpieza de variable
		$comparadors=ModelUsuario::All();
		$logica2=false;
		foreach($comparadors as $comparador)
		{
			if($comparador->RUT == $request->RUT) //comparación)
			{
				$logica2= true;
				break;
			}
		}
		//Se compara logica, si es true significa que el alias ya fue empleado. hay que comparar la negación
		if($logica)
		{
			//Simplemente, se vuelve a validar el uso de ese usuario cuando hay registro previo y si hay registro de borrado en deleted_at
			ModelUsuario::withTrashed()->find($request->RUT)->restore();
			Session::flash('message', 'No se genera nuevo usuario, pero se habilita usuario creado anteriormente.');
			return Redirect::to('main\usuarios\nuevo');
		}
		elseif(!$logica2)
		{
			ModelUsuario::create
			([
				'RUT' =>$request['RUT'],
				'Apellido_P' => $request['Apellido_P'],
				'Apellido_M' => $request['Apellido_M'],
				'Nombres' => $request['Nombres'],
				'Domicilio' =>$request['Domicilio'],
				'Telefono' => $request['Telefono'],
				'Fecha_nacimiento' =>$request['Fecha_nacimiento'],
				'Fecha_ingreso' => $request['Fecha_ingreso'],
				'Rol' => $request['Rol'],
				'password' => $request['password'],
				'email' =>$request['email'],
			]);
			Session::flash('message', 'Nuevo usuario agregado exitosamente');
			return Redirect::to('main/usuarios/nuevo');
		}
		else
		{
			Session::flash('message', 'No se genera nuevo usuario, ya existe uno con el mismo rut.');
			return Redirect::to('main/usuarios/nuevo');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, Request $request)
	{
		try
		{
			$RUT=$request->get('RUT');
			$email=$request->get('email');
			$Apellido_P=$request->get('Apellido_P');
			$Apellido_M=$request->get('Apellido_M');
			$Nombres=$request->get('Nombres');
			$usuarios = ModelUsuario::orderBy('RUT', 'DESC')
				->RUT($RUT)
				->email($email)
				->Apellido_P($Apellido_P)
				->Apellido_M($Apellido_M)
				->Nombres($Nombres)
				->paginate(10);
			return view('sistema.usuarios.administra',compact('usuarios'));
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible filtrar. Dato en uso actualmente.');
			return Redirect::to('main/usuarios/nuevo');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$usuario=ModelUsuario::find($id);
		$celulars=ModelCelular::All();
		return view('sistema.usuarios.edit',['usuario'=>$usuario], compact('celulars'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, UsuarioRequest $request)
	{
		$usuario=ModelUsuario::find($id);
		$usuario->fill($request->all());
		$usuario->save();
		Session::flash('message', 'Edición realizada exitosamente');
		return Redirect::to('main\usuarios\administrar');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try
		{
			//Notar que este método no eliminará nada sino que ocultará regstros.
			$usuario=ModelUsuario::find($id);
			$usuario->delete();
			Session::flash('message', 'Eliminación realizada exitosamente');
			return Redirect::to('main\usuarios\administrar');
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible borrar. Usuario activo en uso actualmente.');
			return Redirect::to('main\ausentismo\administrar');
		}
	}
	
	public function nuevo()
    {
		$celulars=ModelCelular::All();
        return view('sistema.usuarios.nuevo', compact('celulars'));
	}

	public function delete(Request $request)
	{
		try
		{
			//Se recolecta en un arreglo, todas las ID a borrar
			$delets=$request->delete;
			//Evitar errores con un if... en caso que no existan checkboxs seleccionados es decir, no hay $delets
			if($delets)
			{	
				//Se recorre el arreglo $delets... para hacer el borrado
				foreach($delets as $delet)
				{
					$usuario=ModelUsuario::find($delet);
					$usuario->delete();
				}
				Session::flash('message', 'Eliminación masiva realizada exitosamente');
				return Redirect::to('main\usuarios\administrar');
			}
			else
			{
				Session::flash('message', 'No se han efectuado selecciones.');
				return Redirect::to('main\usuarios\administrar');
			}			
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible borrar. Usuario activo en uso actualmente.');
			return Redirect::to('main\usuarios\administrar');
		}
	}

	public function exportaUsuario()
	{
		Excel::create('Usuarios(Listado)', function($excel){
			$excel->sheet('Usuarios', function($sheet){
				$data = ModelUsuario::where('deleted_at', '=', null)->join('Celular', 'Usuario.Telefono', '=', 'Celular.id_celular')
					->select('RUT', 'Apellido_P', 'Apellido_M', 'Nombres', 'Domicilio', 'Numero_celular', 'email', 'Rol')
					->selectRaw("DATE_FORMAT(Fecha_nacimiento,'%d-%m-%Y')")->selectRaw("DATE_FORMAT(Fecha_ingreso,'%d-%m-%Y')")
					->get(); 
				$sheet->fromArray($data);
				$sheet->cell('B1', function($cell) { $cell->setValue('Apellido Paterno'); });
				$sheet->cell('C1', function($cell) { $cell->setValue('Apellido Materno'); });
				$sheet->cell('F1', function($cell) { $cell->setValue('Número Celular'); });
				$sheet->cell('I1', function($cell) { $cell->setValue('Fecha Nacimiento'); });
				$sheet->cell('J1', function($cell) { $cell->setValue('Fecha Ingreso'); });
			});
		})->download('xls');
		return back();
	}	
}
