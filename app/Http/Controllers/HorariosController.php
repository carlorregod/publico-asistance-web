<?php namespace Asistance\Http\Controllers;

use Asistance\Http\Requests;
use Asistance\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Asistance\Http\Requests\HorariosRequest;
//Enlace a la ruta del modelo:
use Asistance\ModelHorarios;
//Para llamar a atributos reservados especiales:
use Illuminate\Support\Facades\Session; 
use Illuminate\Support\Facades\Redirect;
use DB;
use Maatwebsite\Excel\Facades\Excel;

class HorariosController extends Controller {

	/*Protegiendo las rutas asociadas a este controlador */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$horarios= ModelHorarios::orderBy('Alias_horario', 'DESC')
			->paginate(10);
		return view('sistema.horarios.administra',compact('horarios'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(HorariosRequest $request)
	{
		//Primera lógica de guardado: Buscar si el alias de usuario existe o no en el softdelete
		$logica=false;
		$comparadors=ModelHorarios::onlyTrashed()->get(); //sólo valores softdeleteados.
		foreach($comparadors as $comparador)
		{
			if($comparador->Alias_horario == $request->Alias_horario) //comparación)
			{
				$logica= true;
				break;
			}
		}
		//Segunda lógica: buscar el RUT en los valores normales no softdeleteados
		unset($comparadors); //limpieza de variable
		$comparadors=ModelHorarios::All();
		$logica2=false;
		foreach($comparadors as $comparador)
		{
			if($comparador->Alias_horario == $request->Alias_horario) //comparación)
			{
				$logica2= true;
				break;
			}
		}
		//Se compara logica, si es true significa que el alias ya fue empleado. hay que comparar la negación
		if($logica)
		{
			//Simplemente, se vuelve a validar el uso de ese usuario cuando hay registro previo y si hay registro de borrado en deleted_at
			ModelHorarios::withTrashed()-> where('Alias_horario', '=', $request->Alias_horario ) ->restore();
			Session::flash('message', 'No se genera nuevo horario, pero se habilita horario creado anteriormente.');
			return Redirect::to('main\horarios\nuevo');
		}
		//Se compara logica, si es false significa que el alias ya fue empleado.
		elseif(!$logica2)
		{
			//Corrección de los datos atrapados en el checkbox
			$request['Val_lunes'] = (! isset( $request['Val_lunes'] )) ? 0 : 1;
			$request['Val_Martes'] = (! isset( $request['Val_Martes'] )) ? 0 : 1;
			$request['Val_miercoles'] = (! isset( $request['Val_miercoles'] )) ? 0 : 1;
			$request['Val_jueves'] = (! isset( $request['Val_jueves'] )) ? 0 : 1;
			$request['Val_viernes'] = (! isset( $request['Val_viernes'] )) ? 0 : 1;
			$request['Val_sabado'] = (! isset( $request['Val_sabado'] )) ? 0 : 1;
			$request['Val_domingo'] = (! isset( $request['Val_domingo'] )) ? 0 : 1;

			ModelHorarios::create
			([
				'Alias_horario' =>$request['Alias_horario'],
				'Hora_inicio' => $request['Hora_inicio'],
				'Hora_final' => $request['Hora_final'],
				'Val_lunes' => $request['Val_lunes'],
				'Val_Martes' => $request['Val_Martes'],
				'Val_miercoles' => $request['Val_miercoles'],
				'Val_jueves' => $request['Val_jueves'],
				'Val_viernes' => $request['Val_viernes'],
				'Val_sabado' => $request['Val_sabado'],
				'Val_domingo' => $request['Val_domingo'],
			]);
				$request['Val_lunes'] = (! isset( $request['Val_lunes'] )) ? 0 : 1;
				Session::flash('message', 'Nuevo horario generado exitosamente');
				return Redirect::to('main/horarios/nuevo');
		}
		else
		{
			Session::flash('message', 'Horario no generado. Ya existe uno con el mismo nombre.');
			return Redirect::to('main/horarios/nuevo');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, Request $request)
	{
		try
		{
			$Alias_horario=$request->get('Alias_horario');
			//Asignación a variable consulta de todos los elementos de la tabla de la relación "Horarios"			
			$horarios= ModelHorarios::orderBy('Alias_horario', 'DESC')
				->Alias_horario($Alias_horario)
				->paginate(10);
			//Retorno de la vista asociasa a edición de ausentismo
			return view('sistema.horarios.administra',compact('horarios'));
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible filtrar. Dato en uso actualmente.');
			return Redirect::to('main\horarios\administrar');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$horario=ModelHorarios::find($id);
		return view('sistema.horarios.edit',['horario'=>$horario]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, HorariosRequest $request)
	{
			//Corrección de los datos atrapados en el checkbox
			$request['Val_lunes'] = (! isset( $request['Val_lunes'] )) ? 0 : 1;
			$request['Val_Martes'] = (! isset( $request['Val_Martes'] )) ? 0 : 1;
			$request['Val_miercoles'] = (! isset( $request['Val_miercoles'] )) ? 0 : 1;
			$request['Val_jueves'] = (! isset( $request['Val_jueves'] )) ? 0 : 1;
			$request['Val_viernes'] = (! isset( $request['Val_viernes'] )) ? 0 : 1;
			$request['Val_sabado'] = (! isset( $request['Val_sabado'] )) ? 0 : 1;
			$request['Val_domingo'] = (! isset( $request['Val_domingo'] )) ? 0 : 1;
			
			$horario=ModelHorarios::find($id);
			$horario->fill($request->all());
			$horario->save();
			Session::flash('message', 'Edición realizada exitosamente');
			return Redirect::to('main\horarios\administrar');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try
		{
			//Antes de eliminar un horario, hay que asegurarse que éste no esté asignado a un trabajador
			$asignacion = DB::table('Usuario_Horario')->get();
			$logica = true;
			foreach($asignacion as $asigna)
			{
				if($asigna->id_horario == $id)
				{
					$logica = false;
					break;
				}
			}
			//$logica será verdadero en el caso que el horario no esté previamente asignado.
			if($logica)
			{
				$horario=ModelHorarios::find($id);
				$horario->delete();
				Session::flash('message', 'Eliminación realizada exitosamente');
				return Redirect::to('main\horarios\administrar');
			}
			else
			{
				Session::flash('message', 'Horario asignado a uno o varios usuarios. Desasignar antes de borrar.');
				return Redirect::to('main\horarios\administrar');
			}
			
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible borrar. Dato en uso actualmente.');
			return Redirect::to('main\horarios\administrar');
		}
	}

    public function nuevo()
    {
        return view('sistema.horarios.nuevo');
    }

	public function delete(Request $request)
	{
		try
		{
			//Se recolecta en un arreglo, todas las ID a borrar
			$delets=$request->delete;
			//Evitar errores con un if... en caso que no existan checkboxs seleccionados es decir, no hay $delets
			if($delets)
			{	
				//Un primer foreach doble, para revisar que los delets elegidos no estén en uso.
				$asignacion = DB::table('Usuario_Horario')->get();
				$logica = true;	
				foreach($delets as $delet)
				{
					foreach($asignacion as $asigna)
					{
						if($asigna->id_horario == $delet)
						{
							$logica = false;
							break;
						}
					}
				}
				if($logica)
				{
					//Se recorre el arreglo $delets... para hacer el borrado
					foreach($delets as $delet)
					{
						$usuario=ModelHorarios::find($delet);
						$usuario->delete();
					}
					Session::flash('message', 'Eliminación masiva realizada exitosamente');
					return Redirect::to('main\horarios\administrar');
				}
				else
				{
					Session::flash('message', 'Uno o varios horarios seleccionados están asignados a uno o varios usuarios. Desasignar antes del borrado.');
					return Redirect::to('main\horarios\administrar');
				}				
			}
			else
			{
				Session::flash('message', 'No se han efectuado selecciones.');
				return Redirect::to('main\horarios\administrar');
			}
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible borrar. Dato en uso actualmente.');
			return Redirect::to('main\horarios\administrar');
		}		 
	}

	public function exportaHorario()
	{
		Excel::create('Horario(Listado)', function($excel){
			$excel->sheet('Horario', function($sheet){
				$data = ModelHorarios::where('deleted_at', '=', null)
					->select('Alias_horario', 'Hora_inicio', 'Hora_final', 'Val_lunes', 
						'Val_Martes', 'Val_miercoles', 'Val_jueves', 'Val_viernes', 
						'Val_sabado', 'Val_domingo')
					->get(); 
				$sheet->fromArray($data);
			});
		})->download('xls');
		return back();
	}

}
