<?php namespace Asistance\Http\Controllers;

use Asistance\Http\Requests;
use Asistance\Http\Controllers\Controller;

use Illuminate\Http\Request;

//Enlace a la ruta del modelo:
use Asistance\ModelAusentismo;
//ENlazando el request
use Asistance\Http\Requests\AusentismoRequest;
//Para llamar a atributos reservados especiales:
use Illuminate\Support\Facades\Session; 
use Illuminate\Support\Facades\Redirect;
use DB;
use Maatwebsite\Excel\Facades\Excel;

class AusentismoController extends Controller {

	/*Protegiendo las rutas asociadas a este controlador */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$ausentismos= ModelAusentismo::orderBy('Nombre_AusentismoFestivo', 'DESC')
			->paginate(10);
        return view('sistema.ausentismo.administra',compact('ausentismos'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(AusentismoRequest $request)
	{
		//Primera lógica de guardado: Buscar si el alias de ausentismo existe o no en el softdelete
		$logica=false;
		$comparadors=ModelAusentismo::onlyTrashed()->get(); //sólo valores softdeleteados.
		foreach($comparadors as $comparador)
		{
			if($comparador->Nombre_AusentismoFestivo == $request->Nombre_AusentismoFestivo) //comparación)
			{
				$logica= true;
				break;
			}
		}
		//Segunda lógica: buscar el nombre del ausentismo en los valores normales no softdeleteados
		unset($comparadors); //limpieza de variable
		$comparadors=ModelAusentismo::All();
		$logica2=false;
		foreach($comparadors as $comparador)
		{
			if($comparador->Nombre_AusentismoFestivo == $request->Nombre_AusentismoFestivo) //comparación)
			{
				$logica2= true;
				break;
			}
		}
		//Se compara logica, si es true significa que el alias ya fue empleado. hay que comparar la negación
		if($logica)
		{
			//Simplemente, se vuelve a validar el uso de ese usuario cuando hay registro previo y si hay registro de borrado en deleted_at
			ModelAusentismo::withTrashed()-> where('Nombre_AusentismoFestivo', '=', $request->Nombre_AusentismoFestivo ) ->restore();
			Session::flash('message', 'No se genera nuevo ausentismo, pero se habilita ausentismo creado anteriormente.');
			return Redirect::to('main\ausentismo\nuevo');
		}
		//Se compara logica, si es false significa que el alias ya fue empleado.
		elseif(!$logica2)
		{
			ModelAusentismo::create
			([
				'Nombre_AusentismoFestivo' =>$request['Nombre_AusentismoFestivo'],
				'pago' => $request['pago'],
			]);
			Session::flash('message', 'Nuevo ausentismo generado exitosamente');
			return Redirect::to('main/ausentismo/nuevo');
		}
		else
		{
			Session::flash('message', 'No se genera nuevo ausentismo, ya existe uno del mismo nombre');
			return Redirect::to('main/ausentismo/nuevo');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, Request $request)
	{
		try
		{
			$Nombre_AusentismoFestivo=$request->get('Nombre_AusentismoFestivo');
			//Asignación a variable consulta de todos los elementos de la tabla de la relación "Ausentismo"
			$ausentismos=ModelAusentismo::orderBy('Nombre_AusentismoFestivo', 'DESC')
				->Nombre_AusentismoFestivo($Nombre_AusentismoFestivo)
				->paginate(10);
			//Retorno de la vista asociasa a edición de ausentismo
			return view('sistema.ausentismo.administra',compact('ausentismos'));
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible filtrar. Dato en uso actualmente.');
			return Redirect::to('main\ausentismo\administrar');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$ausentismo=ModelAusentismo::find($id);
		return view('sistema.ausentismo.edit',['ausentismo'=>$ausentismo]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, AusentismoRequest $request)
	{
		$ausentismo=ModelAusentismo::find($id);
		$ausentismo->fill($request->all());
		$ausentismo->save();
		Session::flash('message', 'Edición realizada exitosamente');
		return Redirect::to('main\ausentismo\administrar');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try
		{
			//Antes de eliminar un ausentismo, hay que asegurarse que el número no esté asignado a un trabajador
			$asignacion = DB::table('Calendario_AusenFeriado')->get();
			$logica = true;
			foreach($asignacion as $asigna)
			{
				if($asigna->id_ausen_fest == $id)
				{
					$logica = false;
					break;
				}
			}
			//$logica será verdadero en el caso que el ausentismo no esté previamente asignado.
			if($logica)
			{
				//Notar que este método no eliminará nada sino que ocultará regstros.
				$ausentismo=ModelAusentismo::find($id);
				$ausentismo->delete();
				Session::flash('message', 'Eliminación realizada exitosamente');
				return Redirect::to('main\ausentismo\administrar');
			}
			else
			{
				Session::flash('message', 'Ausentismo asignado a uno o varios usuarios. Desasignar antes de borrar.');
				return Redirect::to('main\ausentismo\administrar');
			}
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible borrar. Ausentismo en uso actualmente.');
			return Redirect::to('main\ausentismo\administrar');
		}
	}

	public function nuevo()
    {
		//Para nuevos registros
        return view('sistema.ausentismo.nuevo');
    }

	public function delete(Request $request)
	{
		try
		{
			//Se recolecta en un arreglo, todas las ID a borrar
			$delets=$request->delete;
			//Evitar errores con un if... en caso que no existan checkboxs seleccionados es decir, no hay $delets
			if($delets)
			{	
				//Un primer foreach doble, para revisar que los delets elegidos no estén en uso.
				$asignacion = DB::table('Calendario_AusenFeriado')->get();
				$logica = true;	
				foreach($delets as $delet)
				{
					foreach($asignacion as $asigna)
					{
						if($asigna->id_ausen_fest == $delet)
						{
							$logica = false;
							break;
						}
					}
				}
				if($logica)
				{
					//Se recorre el arreglo $delets... para hacer el borrado
					foreach($delets as $delet)
					{
						$ausentismo=ModelAusentismo::find($delet);
						$ausentismo->delete();
					}
					Session::flash('message', 'Eliminación masiva realizada exitosamente');
					return Redirect::to('main\ausentismo\administrar');
				}
				else
				{
					Session::flash('message', 'Uno o varios de los ausentismos asignados a uno o varios usuarios. Desasignar antes de borrar.');
					return Redirect::to('main\ausentismo\administrar');
				}
			}
			else
			{
				Session::flash('message', 'No se han efectuado selecciones.');
				return Redirect::to('main\ausentismo\administrar');
			}
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible borrar. Ausentismo(s) en uso actualmente.');
			return Redirect::to('main\ausentismo\administrar');
		}
		
	}

	public function exportaAusentismo()
	{
		Excel::create('Ausentismo(Listado)', function($excel){
			$excel->sheet('Ausentismo', function($sheet){
				$data = ModelAusentismo::where('deleted_at', '=', null)
					->select('Nombre_AusentismoFestivo', 'pago')
					->get(); 
				$sheet->fromArray($data);
				$sheet->cell('B1', function($cell) { $cell->setValue('¿Ausentismo Remunerado?'); });
			});
		})->download('xls');
		return back();
	}
}
