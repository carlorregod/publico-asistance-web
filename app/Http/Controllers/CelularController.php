<?php namespace Asistance\Http\Controllers;

use Asistance\Http\Requests;
use Asistance\Http\Controllers\Controller;

use Illuminate\Http\Request;
//Request del celular
use Asistance\Http\Requests\CelularRequest;
//Enlace a la ruta del modelo:
use Asistance\ModelCelular;
//Auxiliarmente, se llamara al modelo usuario
use Asistance\ModelUsuario;
//Para llamar a atributos reservados especiales:
use Illuminate\Support\Facades\Session; 
use Illuminate\Support\Facades\Redirect;
use DB;
use Maatwebsite\Excel\Facades\Excel;

class CelularController extends Controller {

	/*Protegiendo las rutas asociadas a este controlador */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//Asignación a variable consulta de todos los elementos de la tabla de la relación "Celulares"
		$celulars= ModelCelular::orderBy('Numero_celular', 'DESC')
			->paginate(10);
		return view('sistema.usuarios.administracel',compact('celulars'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CelularRequest $request)
	{
		ModelCelular::create
		([
			'Numero_celular' =>$request['Numero_celular'],
			'Compania' => $request['Compania'],
			'IMEI' => $request['IMEI'],
		]);
			Session::flash('message', 'Nuevo celular agregado exitosamente');
			return Redirect::to('main/usuarios/celular');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, Request $request)
	{
		try
		{
			$Numero_celular=$request->get('Numero_celular');
			$celulars= ModelCelular::orderBy('Numero_celular', 'DESC')
				->Numero_celular($Numero_celular)
				->paginate(10);
			return view('sistema.usuarios.administracel',compact('celulars'));
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible filtrar. Dato en uso actualmente.');
			return Redirect::to('main/usuarios/celular');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$celular=ModelCelular::find($id);
		return view('sistema.usuarios.editcel',['celular'=>$celular]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, CelularRequest $request)
	{
		$celular=ModelCelular::find($id);
		$celular->fill($request->all());
		$celular->save();
		Session::flash('message', 'Edición realizada exitosamente');
		return Redirect::to('main\usuarios\administrarcel');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try
		{
			//Antes de eliminar un número de celular, hay que asegurarse que el número no esté asignado a un trabajador
			$usuarios = DB::table('Usuario')->get();
			$logica = true;
			foreach($usuarios as $usuario)
			{
				if($usuario->Telefono == $id)
				{
					$logica = false;
					break;
				}
			}
			//$logica será verdadero en el caso que el celular no esté previamente asignado.
			if($logica)
			{
				ModelCelular::Destroy($id);
				Session::flash('message', 'Eliminación realizada exitosamente');
				return Redirect::to('main\usuarios\administrarcel');
			}
			else
			{
				Session::flash('message', 'Celular asignado a uno o varios usuarios. Desasignar antes de borrar.');
				return Redirect::to('main\usuarios\administrarcel');
			}
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible borrar. Celular activo en registros actualmente.');
			return Redirect::to('main\usuarios\administrarcel');
		}	
	}

	public function celular()
    {
        return view('sistema.usuarios.celular');
	}

	public function delete(Request $request)
	{
		try
		{
			//Se recolecta en un arreglo, todas las ID a borrar
			$delets=$request->delete;
			//Evitar errores con un if... en caso que no existan checkboxs seleccionados es decir, no hay $delets
			if($delets)
			{
				//Un primer foreach doble, para revisar que los delets elegidos no estén en uso.
				$usuarios = DB::table('Usuario')->get();
				$logica = true;
				foreach($delets as $delet)
				{
					foreach($usuarios as $usuario)
					{
						if($usuario->Telefono == $delet)
						{
							$logica = false;
							break;
						}
					}
				}
				if($logica)
				{
					//Se recorre el arreglo $delets... para hacer el borrado
					foreach($delets as $delet)
					{
						DB::table('Celular')->where('id_celular',$delet)->delete();
					}
					Session::flash('message', 'Eliminación masiva realizada exitosamente');
					return Redirect::to('main\usuarios\administrarcel');
				}
				else
				{
					Session::flash('message', 'Uno o varios de los celulares seleccionados están asignados a uno o varios usuarios. Desasignar antes del borrado.');
					return Redirect::to('main\usuarios\administrarcel');
				}
			}
			else
			{
				Session::flash('message', 'No se han efectuado selecciones.');
				return Redirect::to('main\usuarios\administrarcel');
			}
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible borrar. Celular activo en registros actualmente.');
			return Redirect::to('main\usuarios\administrarcel');
		}
	}

	public function exportaCelular()
	{
		Excel::create('Celulares(Listado)', function($excel){
			$excel->sheet('Celulares', function($sheet){
				$data = ModelCelular::select('Numero_celular', 'Compania', 'IMEI')->get(); 
				$sheet->fromArray($data);
			});
		})->download('xls');
		return back();
	}	

}
