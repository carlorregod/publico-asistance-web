<?php namespace Asistance\Http\Controllers;

use Asistance\Http\Requests;
use Asistance\Http\Controllers\Controller;

use Illuminate\Http\Request;
//Llamadas auxiliares
use Auth;
use Asistance\ModelUsuario;
use Illuminate\Support\Facades\Session; 
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;

class MainController extends Controller {
	
	/*Protegiendo las rutas asociadas a este controlador */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('sistema.main.index');
	}

	public function usuarios()
    {
        return view('sistema.main.usuarios');
    }

	public function zonageo()
    {
        return view('sistema.main.zonageo');
    }

	public function ausentismo()
    {
        return view('sistema.main.ausentismo');
    }

	public function horarios()
    {
        return view('sistema.main.horarios');
    }

	public function marcado()
    {
        return view('sistema.main.marcado');
	}
	
	public function cambiapw()
	{
	return view('sistema.main.cambiapw');
	}

	public function pwreset(Request $request)
	{
		try
		{
			if(Auth::user()->RUT == $request->RUT && Auth::user()->email == $request->email)
			{
				if($request->pw1 == $request->pw2)
				{
					//VALOR APROBADO. SE ACTUALIZA EL REGISTRO
					$usuarios=ModelUsuario::All();
					$logica=true;  //Variable que determinará si los datos ingresados son correctos.
					foreach($usuarios as $u)
					{
						if($u->RUT==$request->RUT)
						{
							$usuario=$u;
							break;
						}
					}
					$usuario->fill([
						'password' => $request['pw1'],
					]);
					$usuario->save();
					Session::flash('message', 'Contraseña restablecida. Intente iniciar sesión con su contraseña nueva.');
				}
				else
				{
					Session::flash('message', 'Uno o varios de los datos ingresados se encuentran errados. No se restablece la contraseña.');	
				}
			}
			else
			{
				Session::flash('message', 'Uno o varios de los datos ingresados se encuentran errados. No se restablece la contraseña.');
			}
			return Redirect::to('main/cambiapw');
		}
		catch(\Exception $e)
		{
			//Ya sea porque nunca se formo el registro o por errores varios...
			Session::flash('message', 'Uno o varios de los datos ingresados se encuentran errados. No se restablece la contraseña.');
			return Redirect::to('main/cambiapw');
		}
	}
}
