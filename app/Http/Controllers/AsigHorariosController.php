<?php namespace Asistance\Http\Controllers;

use Asistance\Http\Requests;
use Asistance\Http\Controllers\Controller;

use Illuminate\Http\Request;

//Llamados específicos
use DB;
use Illuminate\Support\Facades\Session; 
use Illuminate\Support\Facades\Redirect;
use Maatwebsite\Excel\Facades\Excel;

use Asistance\AsigHorariosModel; //modelo de la relación
use Asistance\ModelHorarios;//modelo horarios
use Asistance\ModelUsuario;//modelo usuario
use Asistance\Http\Requests\AsigHorariosRequest; //request y restricciones

class AsigHorariosController extends Controller {

	/*Protegiendo las rutas asociadas a este controlador */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//Asignación a variable consulta de todos los elementos de la tabla de la relación "Asignando horarios..."
		$usuarios=ModelUsuario::All();
		$horarios=ModelHorarios::All();
		$asighorarios = DB::table('Usuario_Horario')
		->join('Horario', 'Usuario_Horario.id_horario', '=', 'Horario.id_horario')
		->orderBy('Usuario_Horario.Fecha_inicio', 'desc')
		->paginate(15);
		//Retorno de la vista asociasa a edición de horarios
		return view('sistema.horarios.asignar',compact('asighorarios','usuarios','horarios'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(AsigHorariosRequest $request)
	{
		//Previamente hay que comparar las fechas...
		if($request->Fecha_inicio > $request->Fecha_fin)
		{
			Session::flash('message', 'No se efectuará la asignación. La fecha de inicio no puede ser mayor a la fecha final.');
			return Redirect::to('main/horarios/asignuevo');
		}
		else
		{
			$asighorarios=AsigHorariosModel::All();
			//Fase de comparación de fechas previas con el usuario en cuestión.
			$comparadorlogico= false; //Comparador que determinará si se crea o no el registro en base al usuario y a las asignacioes pasadas
			if(empty($asighorarios))
			{
				$comparadorlogico= true;
			}
			else
			{
				$comparadorlogico= true;
				foreach($asighorarios as $asighorario)
				{
					//Búsqueda de registros pasados con el mismo rut
					if($asighorario->RUT == $request->RUT)
					{
						$comparadorlogico= false;
						break;
					}
				}
				if(!$comparadorlogico)
				{
					$comparadorlogico= true;
					foreach($asighorarios as $asighorario)
					{
						if($asighorario->RUT == $request->RUT)
						{
							if(AsigAusentismoController::check_in_range($asighorario->Fecha_inicio, $asighorario->Fecha_fin, $request->Fecha_fin) ||
							AsigAusentismoController::check_in_range($asighorario->Fecha_inicio, $asighorario->Fecha_fin, $request->Fecha_inicio))
							{
								$comparadorlogico= false;
								break;
							}
						}
					}
				}	
			}	
			if ($comparadorlogico)
			{
				AsigHorariosModel::create
				([
					'id_horario' =>$request['id_horario'],
					'RUT' => $request['RUT'],
					'Fecha_inicio' =>$request['Fecha_inicio'],
					'Fecha_fin' =>$request['Fecha_fin'],
				]);
					Session::flash('message', 'Nueva asignacion de horario generada exitosamente');
					return Redirect::to('main/horarios/asignuevo');
			}
			else
			{
				Session::flash('message', 'Rango de fechas especificadas abarcan una o varias fechas preasignadas. No se realiza asignación.');
				return Redirect::to('main/horarios/asignuevo');
			}
		}			
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, Request $request)
	{
		try
		{
			if(strtotime($request->fechaInicio) > strtotime($request->fechaFinal))
			{
				//Asignación a variable consulta de todos los elementos de la tabla de la relación "Asignando horarios..."
				$usuarios=ModelUsuario::All();
				$horarios=ModelHorarios::All();
				$asighorarios = DB::table('Usuario_Horario')
				->join('Horario', 'Usuario_Horario.id_horario', '=', 'Horario.id_horario')
				->orderBy('Usuario_Horario.Fecha_inicio', 'desc')
				->paginate(15);
				//Retorno de la vista asociasa a edición de horarios
				Session::flash('message', 'La fecha de inicio de la búsqueda no puede ser mayor a la fecha final.');
				return view('sistema.horarios.asignar',compact('asighorarios','usuarios','horarios'));
			}
			else
			{
				//Asignación a variable consulta de todos los elementos de la tabla de la relación "Asignando horarios..."
				$usuarios=ModelUsuario::All();
				$horarios=ModelHorarios::All();
				$asighorarios = DB::table('Usuario_Horario')
					->join('Horario', 'Usuario_Horario.id_horario', '=', 'Horario.id_horario')
					->where('Usuario_Horario.RUT', '=', $request->RUT)
					->whereBetween('Usuario_Horario.Fecha_inicio', [$request->fechaInicio, $request->fechaFinal])
					->orderBy('Usuario_Horario.Fecha_inicio', 'desc')
					->paginate(15);
				//Retorno de la vista asociasa a edición de horarios
				return view('sistema.horarios.asignar',compact('asighorarios','usuarios','horarios'));
			}
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Datos en uso actualmente, imposibe filtrar.');
			return Redirect::to('main/horarios/asigadministrar');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$usuarios=ModelUsuario::All();
		$horarios=ModelHorarios::All();
		$asighorario=AsigHorariosModel::find($id);
		return view('sistema.horarios.asigedit',['asighorario'=>$asighorario], compact('usuarios', 'horarios'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, AsigHorariosRequest $request)
	{
		//Hay que comparar las fechas...
		if($request->Fecha_inicio > $request->Fecha_fin)
		{
			Session::flash('message', 'No se efectuará la edición. La fecha de inicio no puede ser mayor a la fecha final.');
			return Redirect::to('main/horarios/asigadministrar');
		}
		else
		{
			$asighorarios=AsigHorariosModel::All();
			$comparadorlogico= true;
			foreach($asighorarios as $asighorario)
			{
				if($asighorario->RUT == $request->RUT)
				{
					if(AsigAusentismoController::check_in_range($asighorario->Fecha_inicio, $asighorario->Fecha_fin, $request->Fecha_fin) ||
					AsigAusentismoController::check_in_range($asighorario->Fecha_inicio, $asighorario->Fecha_fin, $request->Fecha_inicio))
					{
						$comparadorlogico= false;
						break;
					}
				}
			}		
			if ($comparadorlogico)
			{
				$asighorario=AsigHorariosModel::find($id);
				$asighorario->fill($request->all());
				$asighorario->save();
				Session::flash('message', 'Edición realizada exitosamente');
				return Redirect::to('main/horarios/asigadministrar');
			}
			else
			{
				Session::flash('message', 'Rango de fechas especificadas abarcan una o varias fechas preasignadas. No se realiza actualización.');
				return Redirect::to('main/horarios/asigadministrar');
			}
		}	
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try
		{
			AsigHorariosModel::Destroy($id);
			Session::flash('message', 'Eliminación realizada exitosamente');
			return Redirect::to('main\horarios\asigadministrar');
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible borrar. Dato en uso actualmente. Desasgne primero.');
			return Redirect::to('main\horarios\asigadministrar');
		}	
	}

	public function delete(Request $request)
	{
		try
		{
			//Se recolecta en un arreglo, todas las ID a borrar
			$delets=$request->delete;
			//Evitar errores con un if... en caso que no existan checkboxs seleccionados es decir, no hay $delets
			if($delets)
			{	
				//Se recorre el arreglo $delets... para hacer el borrado
				foreach($delets as $delet)
				{
					DB::table('Usuario_Horario')->where('contador',$delet)->delete();
				}
				Session::flash('message', 'Eliminación masiva realizada exitosamente');
				return Redirect::to('main\horarios\asigadministrar');
			}
			else
			{
				Session::flash('message', 'No se han efectuado selecciones.');
				return Redirect::to('main\horarios\asigadministrar');
			}
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible borrar. Uno o varios datos en uso actualmente. Desasgne primero.');
			return Redirect::to('main\horarios\asigadministrar');
		}				
	}

	public function nuevo()
	{
		//Para nuevas asociaciones...
		$usuarios=ModelUsuario::All();
		$horarios=ModelHorarios::All();
		return view('sistema.horarios.asignuevo', compact('usuarios', 'horarios'));
	}

	public function exportaHorario(Request $request)
	{
		Excel::create('Asignacion_Horarios(Listado)', function($excel) use($request){
			$excel->sheet('Horario_Asig', function($sheet) use($request){
				$data = AsigHorariosModel::join('Horario', 'Usuario_Horario.id_horario', '=', 'Horario.id_horario')
					->whereBetween('Usuario_Horario.Fecha_inicio', [$request->fechaInicio, $request->fechaFinal])
					->select('RUT', 'Alias_horario', 'Hora_inicio', 'Hora_final', 'Val_lunes', 
					'Val_Martes', 'Val_miercoles', 'Val_jueves', 'Val_viernes', 
					'Val_sabado', 'Val_domingo')
					->selectRaw("DATE_FORMAT(Fecha_inicio,'%d-%m-%Y')")
					->selectRaw("DATE_FORMAT(Fecha_fin,'%d-%m-%Y')")
					->get(); 
				$sheet->fromArray($data);
				$sheet->cell('L1', function($cell) { $cell->setValue('Fecha Inicial'); });
				$sheet->cell('M1', function($cell) { $cell->setValue('Fecha Final'); });
			});
		})->download('xls');
		return back();
	}

}
