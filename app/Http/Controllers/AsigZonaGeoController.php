<?php namespace Asistance\Http\Controllers;

use Asistance\Http\Requests;
use Asistance\Http\Controllers\Controller;

use Illuminate\Http\Request;

//Llamados específicos
use DB;
use Illuminate\Support\Facades\Session; 
use Illuminate\Support\Facades\Redirect;
use Maatwebsite\Excel\Facades\Excel;

use Asistance\AsigZonaGeoModel; //modelo de la relación
use Asistance\ModelGeolocalizacion;//modelo zonas geográficas
use Asistance\ModelTipoMarca; //modelo de los tipos de marcas, entrada, salida, ...
use Asistance\ModelUsuario;//modelo usuario
use Asistance\Http\Requests\AsigZonaGeoRequest; //request y restricciones

class AsigZonaGeoController extends Controller {

	/*Protegiendo las rutas asociadas a este controlador */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//Asignación a variable consulta de todos los elementos de la tabla de la relación "Asignando zona geográfica..."
		$usuarios=ModelUsuario::All();
		$zonageos=ModelGeolocalizacion::All();
		$tipomarcas=ModelTipoMarca::All();
		$asigzonageos = DB::table('Asignacion_loc_geo')
			->join('Localizacion_geo', 'Asignacion_loc_geo.id_loc_geo', '=', 'Localizacion_geo.id_loc_geo')
			->join('Marcado', 'Asignacion_loc_geo.id_marcado', '=', 'Marcado.id_marcado')
			->orderBy('Asignacion_loc_geo.Fecha_inicio', 'desc')
			->paginate(15);
		//Retorno de la vista asociasa a edición de zonas geográficas
		return view('sistema.zonageo.asignar',compact('asigzonageos','usuarios','zonageos','tipomarcas'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(AsigZonaGeoRequest $request)
	{
		//Previamente hay que comparar las fechas...
		if($request->Fecha_inicio > $request->Fecha_fin)
			{
			Session::flash('message', 'No se efectuará la asignación. La fecha de inicio no puede ser mayor a la fecha final.');
			return Redirect::to('main/zonageo/asignuevo');
			}
		else
		{

			$asigzonageos=AsigZonaGeoModel::All();
			//Fase de comparación de fechas previas con el usuario en cuestión.
			$comparadorlogico= false; //Comparador que determinará si se crea o no el registro en base al usuario y a las asignacioes pasadas
			if(empty($asigzonageos))
			{
				$comparadorlogico= true;
			}
			else
			{
				$comparadorlogico= true;
				foreach($asigzonageos as $asigzonageo)
				{
					//Búsqueda de registros pasados con el mismo rut
					if($asigzonageo->RUT == $request->RUT)
					{
						$comparadorlogico= false;
						break;
					}
				}
				if(!$comparadorlogico)
				{
					$comparadorlogico= true;
					foreach($asigzonageos as $asigzonageo)
					{
						if($asigzonageo->RUT == $request->RUT)
						{
							if( (AsigAusentismoController::check_in_range($asigzonageo->Fecha_inicio, $asigzonageo->Fecha_fin, $request->Fecha_fin) ||
							AsigAusentismoController::check_in_range($asigzonageo->Fecha_inicio, $asigzonageo->Fecha_fin, $request->Fecha_inicio)) &&
							$asigzonageo->id_marcado == $request->id_marcado )
							{
								$comparadorlogico= false;
								break;
							}
						}
					}
				}	
			}	
			if ($comparadorlogico)
			{
				AsigZonaGeoModel::create
				([
					'RUT' => $request['RUT'],
					'id_marcado' =>$request['id_marcado'],
					'id_loc_geo' =>$request['id_loc_geo'],
					'Fecha_inicio' =>$request['Fecha_inicio'],
					'Fecha_fin' =>$request['Fecha_fin'],
				]);
					Session::flash('message', 'Nueva asignacion de zona geográfica generada exitosamente');
					return Redirect::to('main/zonageo/asignuevo');
			}
			else
			{
				Session::flash('message', 'Rango de fechas especificadas abarcan una o varias fechas preasignadas en un mismo tipo de marcado. No se realiza asignación.');
				return Redirect::to('main/zonageo/asignuevo');
			}
		}		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, Request $request)
	{
		try
		{
			if(strtotime($request->fechaInicio) > strtotime($request->fechaFinal))
			{
				$usuarios=ModelUsuario::All();
				$zonageos=ModelGeolocalizacion::All();
				$tipomarcas=ModelTipoMarca::All();
				$asigzonageos = DB::table('Asignacion_loc_geo')
					->join('Localizacion_geo', 'Asignacion_loc_geo.id_loc_geo', '=', 'Localizacion_geo.id_loc_geo')
					->join('Marcado', 'Asignacion_loc_geo.id_marcado', '=', 'Marcado.id_marcado')
					->orderBy('Asignacion_loc_geo.Fecha_inicio', 'desc')
					->paginate(15);
				//Retorno de la vista asociasa a edición de zonas geográficas
				Session::flash('message', 'La fecha de inicio de la búsqueda no puede ser mayor a la fecha final.');
				return view('sistema.zonageo.asignar',compact('asigzonageos','usuarios','zonageos','tipomarcas'));
			}
			else
			{
				//Asignación a variable consulta de todos los elementos de la tabla de la relación "Asignando zona geográfica..."
				$usuarios=ModelUsuario::All();
				$zonageos=ModelGeolocalizacion::All();
				$tipomarcas=ModelTipoMarca::All();
				$asigzonageos = DB::table('Asignacion_loc_geo')
					->join('Localizacion_geo', 'Asignacion_loc_geo.id_loc_geo', '=', 'Localizacion_geo.id_loc_geo')
					->join('Marcado', 'Asignacion_loc_geo.id_marcado', '=', 'Marcado.id_marcado')
					->where('Asignacion_loc_geo.RUT', '=', $request->RUT)
					->whereBetween('Asignacion_loc_geo.Fecha_inicio', [$request->fechaInicio, $request->fechaFinal])
					->orderBy('Asignacion_loc_geo.Fecha_inicio', 'desc')
					->paginate(15);
				//Retorno de la vista asociasa a edición de zonas geográficas
				return view('sistema.zonageo.asignar',compact('asigzonageos','usuarios','zonageos','tipomarcas'));
			}
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Datos en uso actualmente, imposibe filtrar.');
			return Redirect::to('main/zonageo/asigadministrar');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$usuarios=ModelUsuario::All();
		$zonageos=ModelGeolocalizacion::All();
		$tipomarcas=ModelTipoMarca::All();
		$asigzonageo=AsigZonaGeoModel::find($id);
		return view('sistema.zonageo.asigedit',['asigzonageo'=>$asigzonageo], compact('usuarios', 'zonageos', 'tipomarcas'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, AsigZonaGeoRequest $request)
	{
		//Hay que comparar las fechas...
		if($request->Fecha_inicio > $request->Fecha_fin)
		{
				Session::flash('message', 'No se efectuará la edición. La fecha de inicio no puede ser mayor a la fecha final.');
				return Redirect::to('main/zonageo/asigadministrar');
		}
		else
		{
			$asigzonageos=AsigZonaGeoModel::All();
			$comparadorlogico= true;
			foreach($asigzonageos as $asigzonageo)
			{
				if($asigzonageo->RUT == $request->RUT)
				{
					if( (AsigAusentismoController::check_in_range($asigzonageo->Fecha_inicio, $asigzonageo->Fecha_fin, $request->Fecha_fin) ||
						AsigAusentismoController::check_in_range($asigzonageo->Fecha_inicio, $asigzonageo->Fecha_fin, $request->Fecha_inicio)) &&
						$asigzonageo->id_marcado == $request->id_marcado )
					{
						$comparadorlogico= false;
						break;
					}
				}
			}		
			if ($comparadorlogico)
			{
				$asigzonageo=AsigZonaGeoModel::find($id);
				$asigzonageo->fill($request->all());
				$asigzonageo->save();
				Session::flash('message', 'Edición realizada exitosamente');
				return Redirect::to('main/zonageo/asigadministrar');
			}
			else
			{
				Session::flash('message', 'Rango de fechas especificadas abarcan una o varias fechas preasignadas en un mismo tipo de marcado. No se realiza actualización.');
				return Redirect::to('main/zonageo/asigadministrar');
			}
		}	
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try
		{
			AsigZonaGeoModel::Destroy($id);
			Session::flash('message', 'Eliminación realizada exitosamente');
			return Redirect::to('main\zonageo\asigadministrar');
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible borrar. Dato en uso actualmente. Desasgne primero.');
			return Redirect::to('main\zonageo\asigadministrar');
		}		
	}

	public function delete(Request $request)
	{
		try
		{
			//Se recolecta en un arreglo, todas las ID a borrar
			$delets=$request->delete;
			//Evitar errores con un if... en caso que no existan checkboxs seleccionados es decir, no hay $delets
			if($delets)
			{	
				//Se recorre el arreglo $delets... para hacer el borrado
				foreach($delets as $delet)
				{
					DB::table('Asignacion_loc_geo')->where('contador',$delet)->delete();
				}
				Session::flash('message', 'Eliminación masiva realizada exitosamente');
				return Redirect::to('main\zonageo\asigadministrar');
			}
			else
			{
				Session::flash('message', 'No se han efectuado selecciones.');
				return Redirect::to('main\zonageo\asigadministrar');
			}
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible borrar. Uno o varios datos en uso actualmente. Desasgne primero.');
			return Redirect::to('main\zonageo\asigadministrar');
		}			
	}

	public function nuevo()
	{
		//Para nuevas asociaciones...
		$usuarios=ModelUsuario::All();
		$zonageos=ModelGeolocalizacion::All();
		$tipomarcas=ModelTipoMarca::All();
		return view('sistema.zonageo.asignuevo', compact('usuarios', 'zonageos', 'tipomarcas'));
	}

	public function exportaZonaGeo(Request $request)
	{
		Excel::create('Asignacion_Zona_Geografica(Listado)', function($excel) use($request){
			$excel->sheet('Zonageo_Asig', function($sheet) use($request){
				$data = AsigZonaGeoModel::join('Localizacion_geo', 'Asignacion_loc_geo.id_loc_geo', '=', 'Localizacion_geo.id_loc_geo')
				->join('Marcado', 'Asignacion_loc_geo.id_marcado', '=', 'Marcado.id_marcado')
				->whereBetween('Asignacion_loc_geo.Fecha_inicio', [$request->fechaInicio, $request->fechaFinal])
				->select('RUT', 'Tipo_marca', 'coorX1', 'coorY1', 'coorX2', 'coorY2')
				->selectRaw("DATE_FORMAT(Fecha_inicio,'%d-%m-%Y')")
				->selectRaw("DATE_FORMAT(Fecha_fin,'%d-%m-%Y')")
				->get(); 
				$sheet->fromArray($data);
				$sheet->cell('G1', function($cell) { $cell->setValue('Fecha Inicial'); });
				$sheet->cell('H1', function($cell) { $cell->setValue('Fecha Final'); });
			});
		})->download('xls');
		return back();
	}
}
