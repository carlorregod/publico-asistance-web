<?php namespace Asistance\Http\Controllers;

use Asistance\Http\Requests;
use Asistance\Http\Controllers\Controller;

use Illuminate\Http\Request;
//Llamados específicos
use DB;
use Illuminate\Support\Facades\Session; 
use Illuminate\Support\Facades\Redirect;
use Maatwebsite\Excel\Facades\Excel;

use Asistance\ModelMarcado; //modelo de la relación
use Asistance\ModelCelular;//modelo celular
use Asistance\ModelUsuario;//modelo usuario
use Asistance\ModelTipoMarca;//modelo tipo de marca
use Asistance\ModelHorarios;//modelo tipo de horarios
use Asistance\ModelGeolocalizacion;//modelo de las geolocalizaciones
use Asistance\Http\Requests\MarcadoRequest; //request y restricciones

class MarcadoController extends Controller {

	/*Protegiendo las rutas asociadas a este controlador */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$usuarios=ModelUsuario::All();
		//Confección de la vista de tabla de las marcas de asistencia
		$marcasasistencias = ModelMarcado::join('Usuario', 'Registro_Asistencia.RUT', '=','Usuario.RUT')
			->join('Celular', 'Registro_Asistencia.id_celular', '=', 'Celular.id_celular')
			->join('Marcado', 'Registro_Asistencia.id_marcado', '=', 'Marcado.id_marcado')
			->orderBy('Fecha_marcado', 'desc')
			->paginate(15);
		//Retorno de la vista asociasa a edición de las marcas de asistencia
		return view('sistema.marcado.administrar',compact('marcasasistencias', 'usuarios'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(MarcadoRequest $request)
	{
		try
		{
			ModelMarcado::create
			([
				'RUT' =>$request['RUT'],
				'id_marcado' => $request['id_marcado'],
				'id_celular' => $request['id_celular'],
				'Fecha_marcado' => $request['Fecha_marcado'],
				'Hora_marca' => $request['Hora_marca'],
				'coordX_GPS' => $request['coordX_GPS'],
				'coordY_GPS' => $request['coordY_GPS'],
				'Val_viernes' => $request['Val_viernes'],
				'estado_ausentismo' => 'Presente',
			]);
				Session::flash('message', 'Marca de urgencia realizada exitosamente');
				return Redirect::to('main/marcado/marcau');
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible generar. Dato en uso actualmente.');
			return Redirect::to('main/marcado/marcau');
		}		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, Request $request)
	{
		try
		{
			if(strtotime($request->fechaInicio) > strtotime($request->fechaFinal))
			{
				$usuarios=ModelUsuario::All();
				//Confección de la vista de tabla de las marcas de asistencia
				$marcasasistencias = ModelMarcado::join('Usuario', 'Registro_Asistencia.RUT', '=','Usuario.RUT')
					->join('Celular', 'Registro_Asistencia.id_celular', '=', 'Celular.id_celular')
					->join('Marcado', 'Registro_Asistencia.id_marcado', '=', 'Marcado.id_marcado')
					->orderBy('Fecha_marcado', 'desc')
					->paginate(15);
				//Retorno de la vista asociasa a edición de zonas geográficas
				Session::flash('message', 'La fecha inicio del rango de búsqueda debe ser superior a la fecha final.');
				return view('sistema.marcado.administrar',compact('marcasasistencias', 'usuarios'));
			}
			else
			{
				$usuarios=ModelUsuario::All();
				//Confección de la vista de tabla de las marcas de asistencia
				$marcasasistencias = ModelMarcado::join('Usuario', 'Registro_Asistencia.RUT', '=','Usuario.RUT')
					->join('Celular', 'Registro_Asistencia.id_celular', '=', 'Celular.id_celular')
					->join('Marcado', 'Registro_Asistencia.id_marcado', '=', 'Marcado.id_marcado')
					->where('Registro_Asistencia.RUT', '=', $request->RUT)
					->whereBetween('Registro_Asistencia.Fecha_marcado', [$request->fechaInicio, $request->fechaFinal])
					->orderBy('Fecha_marcado', 'desc')
					->paginate(15);
				//Retorno de la vista asociasa a edición de zonas geográficas
				return view('sistema.marcado.administrar',compact('marcasasistencias', 'usuarios'));
			}	
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible filtrar. Registro en uso activo.');
			return Redirect::to('main\marcado\administrar');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function marcau()
    {
		$usuarios=ModelUsuario::All();
		$celulars=ModelCelular::All();
		$marcados=ModelTipoMarca::All();
        return view('sistema.marcado.marcau', compact('usuarios', 'celulars', 'marcados'));
	}

	public function delete(Request $request)
	{
		try
		{
			//Se recolecta en un arreglo, todas las ID a borrar
			$delets=$request->delete;
			//Evitar errores con un if... en caso que no existan checkboxs seleccionados es decir, no hay $delets
			if($delets)
			{	
				//Se recorre el arreglo $delets... para hacer el borrado
				foreach($delets as $delet)
				{
					$marcado=ModelMarcado::find($delet);
					$marcado->delete();
				}
				Session::flash('message', 'Eliminación masiva realizada exitosamente');
				return Redirect::to('main\marcado\administrar');
			}
			else
			{
				Session::flash('message', 'No se han efectuado selecciones.');
				return Redirect::to('main\marcado\administrar');
			}
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible borrar. Registro en uso activo.');
			return Redirect::to('main\marcado\administrar');
		}
	}

	public function exportaMarcas(Request $request)
	{
		Excel::create('Marcas_Asistencia(Listado)', function($excel) use($request){
			$excel->sheet('Marcados', function($sheet) use($request){
				$data = ModelMarcado::join('Usuario', 'Registro_Asistencia.RUT', '=','Usuario.RUT')
					->join('Celular', 'Registro_Asistencia.id_celular', '=', 'Celular.id_celular')
					->join('Marcado', 'Registro_Asistencia.id_marcado', '=', 'Marcado.id_marcado')
					->whereBetween('Registro_Asistencia.Fecha_marcado', [$request->fechaInicio, $request->fechaFinal])					
					->select('Usuario.RUT', 'Apellido_P', 'Apellido_M', 'Nombres', 'Numero_celular', 'IMEI',
						'Tipo_marca', 'coordX_GPS', 'coordY_GPS', 'Hora_marca')
					->selectRaw("DATE_FORMAT(Fecha_marcado,'%d-%m-%Y')")
					->get(); 
				$sheet->fromArray($data);
				$sheet->cell('B1', function($cell) { $cell->setValue('Apellido Paterno'); });
				$sheet->cell('C1', function($cell) { $cell->setValue('Apellido Materno'); });
				$sheet->cell('E1', function($cell) { $cell->setValue('Número Celular'); });
				$sheet->cell('E1', function($cell) { $cell->setValue('Número Celular'); });
				$sheet->cell('G1', function($cell) { $cell->setValue('Tipo de Marcado'); });
				$sheet->cell('J1', function($cell) { $cell->setValue('Hora de Marcado'); });
				$sheet->cell('K1', function($cell) { $cell->setValue('Fecha de Marcado'); });
			});
		})->download('xls');
		return back();
	}
}