<?php namespace Asistance\Http\Controllers;

use Asistance\Http\Requests;
use Asistance\Http\Controllers\Controller;
//Enlace a la ruta del modelo:
use Asistance\ModelUsuario;
use Illuminate\Http\Request;

use Asistance\Http\Requests\LoginRequest;
use Auth;
use DB;

use Illuminate\Support\Facades\Session; 
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('sistema.login.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	public function store(LoginRequest $request)
	{
		//Verificador que comprobará el rol del usuario. Un Trabajador no podrá acceder al menú principal.
		$verificador = false;
		$usuarios=ModelUsuario::All();
		foreach($usuarios as $usuario)
		{
			if($usuario->RUT == $request->RUT && $usuario->Rol == 'Trabajador')
			{
				$verificador= true ;
			}
		}
		if ($verificador)
		{
			Session::flash('message-errors', 'Inicio de sesión incorrecto, revise su RUT y/o contraseña. ');
			return Redirect::to('/');
		}
		else
		{
			$credentials = [
				'RUT' => $request['RUT'],
				'password' => $request['password']
			];
			if(Auth::attempt($credentials))
			{
				return Redirect::to('main');
			}
			else
			{
				Session::flash('message-errors', 'Inicio de sesión incorrecto, revise su RUT y/o contraseña. ');
				return Redirect::to('/');
			}
		}	
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function logout()
	{
		Auth::logout(); 
		return Redirect::to('/');
	}
		
}
