<?php namespace Asistance\Http\Controllers;

use Asistance\Http\Requests;
use Asistance\Http\Controllers\Controller;

use Illuminate\Http\Request;
//Llamados específicos
use DB;
use Illuminate\Support\Facades\Session; 
use Illuminate\Support\Facades\Redirect;
use Maatwebsite\Excel\Facades\Excel;

use Asistance\AsigAusentismoModel; //modelo de la relación
use Asistance\ModelAusentismo;//modelo ausentismo
use Asistance\ModelUsuario;//modelo usuario
use Asistance\Http\Requests\AsigAusentismoRequest; //request y restricciones

class AsigAusentismoController extends Controller {

	/*Protegiendo las rutas asociadas a este controlador */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//Asignación a variable consulta de todos los elementos de la tabla de la relación "Asignando ausentismos..."
		$usuarios=ModelUsuario::All();
		$ausentismos=ModelAusentismo::All();
		$asigausentismos = DB::table('Calendario_AusenFeriado')
			->join('AusentismoFestivo', 'Calendario_AusenFeriado.id_ausen_fest', '=', 'AusentismoFestivo.id_ausen_fest')
			->orderBy('Calendario_AusenFeriado.Fecha_inicio', 'desc')
			->paginate(15);
		//Retorno de la vista asociasa a edición de ausentismo
		return view('sistema.ausentismo.asignar',compact('asigausentismos','usuarios','ausentismos'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(AsigAusentismoRequest $request)
	{
		$asigausentismos=AsigAusentismoModel::All();
		//Previamente hay que comparar las fechas...
		if($request->Fecha_inicio > $request->Fecha_fin)
			{
			Session::flash('message', 'No se efectuará la asignación. La fecha de inicio no puede ser mayor a la fecha final.');
			return Redirect::to('main/ausentismo/asignuevo');
			}
		else
		{
			//Fase de comparación de fechas previas con el usuario en cuestión.
			$comparadorlogico= false; //Comparador que determinará si se crea o no el registro en base al usuario y a las asignacioes pasadas
			if(empty($asigausentismos))
			{
				$comparadorlogico= true;
			}
			else
			{
				$comparadorlogico= true;
				foreach($asigausentismos as $asigausentismo)
				{
					//Búsqueda de registros pasados con el mismo rut
					if($asigausentismo->RUT == $request->RUT)
					{
						$comparadorlogico= false;
						break;
					}
				}
				if(!$comparadorlogico)
				{
					$comparadorlogico= true;
					foreach($asigausentismos as $asigausentismo)
					{
						if($asigausentismo->RUT == $request->RUT)
						{
							if(AsigAusentismoController::check_in_range($asigausentismo->Fecha_inicio, $asigausentismo->Fecha_fin, $request->Fecha_fin) ||
							AsigAusentismoController::check_in_range($asigausentismo->Fecha_inicio, $asigausentismo->Fecha_fin, $request->Fecha_inicio))
							{
								$comparadorlogico= false;
								break;
							}
						}
					}
				}	
			}	
			if ($comparadorlogico)
			{
				AsigAusentismoModel::create
				([
				'id_ausen_fest' =>$request['id_ausen_fest'],
				'RUT' => $request['RUT'],
				'Fecha_inicio' =>$request['Fecha_inicio'],
				'Fecha_fin' =>$request['Fecha_fin'],
				]);
				Session::flash('message', 'Nueva asignacion de ausentismo generada exitosamente');
				return Redirect::to('main/ausentismo/asignuevo');
			}
			else
			{
				Session::flash('message', 'Rango de fechas especificadas abarcan una o varias fechas preasignadas. No se realiza asignación.');
				return Redirect::to('main/ausentismo/asignuevo');
			}
		}		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, Request $request)
	{
		try
		{
			if(strtotime($request->fechaInicio) > strtotime($request->fechaFinal))
			{
				$usuarios=ModelUsuario::All();
				$ausentismos=ModelAusentismo::All();
				$asigausentismos = DB::table('Calendario_AusenFeriado')
					->join('AusentismoFestivo', 'Calendario_AusenFeriado.id_ausen_fest', '=', 'AusentismoFestivo.id_ausen_fest')
					->orderBy('Calendario_AusenFeriado.Fecha_inicio', 'desc')
					->paginate(15);
				//Retorno de la vista asociasa a edición de ausentismo
				Session::flash('message', 'La fecha de inicio de la búsqueda no puede ser mayor a la fecha final.');
				return view('sistema.ausentismo.asignar',compact('asigausentismos','usuarios','ausentismos'));
			}
			else
			{
				$usuarios=ModelUsuario::All();
				$ausentismos=ModelAusentismo::All();
				$asigausentismos = DB::table('Calendario_AusenFeriado')
					->join('AusentismoFestivo', 'Calendario_AusenFeriado.id_ausen_fest', '=', 'AusentismoFestivo.id_ausen_fest')
					->where('Calendario_AusenFeriado.RUT', '=', $request->RUT)
					->whereBetween('Calendario_AusenFeriado.Fecha_inicio', [$request->fechaInicio, $request->fechaFinal])
					->orderBy('Calendario_AusenFeriado.Fecha_inicio', 'desc')
					->paginate(15);
				//Retorno de la vista asociasa a edición de ausentismo
				return view('sistema.ausentismo.asignar',compact('asigausentismos','usuarios','ausentismos'));
			}	
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Datos en uso actualmente, imposibe filtrar.');
			return Redirect::to('main/zonageo/asigadministrar');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id1
	 * @return Response
	 */
	public function edit($id)
	{
		$usuarios=ModelUsuario::All();
		$ausentismos=ModelAusentismo::All();
		$asigausentismo=AsigAusentismoModel::find($id);
		return view('sistema.ausentismo.asigedit',['asigausentismo'=>$asigausentismo], compact('usuarios', 'ausentismos'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, AsigAusentismoRequest $request)
	{
		$asigausentismos=AsigAusentismoModel::All();
		//Hay que comparar las fechas...
		if($request->Fecha_inicio > $request->Fecha_fin)
		{
			Session::flash('message', 'No se efectuará la edición. La fecha de inicio no puede ser mayor a la fecha final.');
			return Redirect::to('main/ausentismo/asigadministrar');
		}
		else
		{
			$comparadorlogico= true;
			foreach($asigausentismos as $asigausentismo)
			{
				if($asigausentismo->RUT == $request->RUT)
				{
					if(AsigAusentismoController::check_in_range($asigausentismo->Fecha_inicio, $asigausentismo->Fecha_fin, $request->Fecha_fin) ||
					AsigAusentismoController::check_in_range($asigausentismo->Fecha_inicio, $asigausentismo->Fecha_fin, $request->Fecha_inicio))
					{
						$comparadorlogico= false;
						break;
					}
				}
			}		
			if ($comparadorlogico)
			{
			$asigausentismo=AsigAusentismoModel::find($id);
			$asigausentismo->fill($request->all());
			$asigausentismo->save();
			Session::flash('message', 'Edición realizada exitosamente');
			return Redirect::to('main\ausentismo\asigadministrar');
			}
			else
			{
				Session::flash('message', 'Rango de fechas especificadas abarcan una o varias fechas preasignadas. No se realiza actualización.');
				return Redirect::to('main/ausentismo/asigadministrar');
			}
		}	
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try
		{
			AsigAusentismoModel::Destroy($id);
			Session::flash('message', 'Eliminación realizada exitosamente');
			return Redirect::to('main\ausentismo\asigadministrar');
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible borrar. Dato en uso actualmente. Desasgne primero.');
			return Redirect::to('main\ausentismo\asigadministrar');
		}			
	}

	public function delete(Request $request)
	{
		try
		{
			//Se recolecta en un arreglo, todas las ID a borrar
			$delets=$request->delete;
			//Evitar errores con un if... en caso que no existan checkboxs seleccionados es decir, no hay $delets
			if($delets)
			{	
				//Se recorre el arreglo $delets... para hacer el borrado
				foreach($delets as $delet)
				{
					DB::table('Calendario_AusenFeriado')->where('contador',$delet)->delete();
				}
				Session::flash('message', 'Eliminación masiva realizada exitosamente');
				return Redirect::to('main\ausentismo\asigadministrar');
			}
			else
			{
				Session::flash('message', 'No se han efectuado selecciones.');
				return Redirect::to('main\ausentismo\asigadministrar');
			}
		}
		catch(\Exception $e)
		{
			Session::flash('message', 'Imposible borrar. Uno o varios datos en uso actualmente. Desasgne primero.');
			return Redirect::to('main\ausentismo\asigadministrar');
		}		
	}

	public function nuevo()
	{
		//Para nuevas asociaciones...
		$usuarios=ModelUsuario::All();
		$ausentismos=ModelAusentismo::All();
		return view('sistema.ausentismo.asignuevo', compact('usuarios', 'ausentismos'));
	}

	public function exportaAusentismo(Request $request)
	{
		Excel::create('Asignacion_Ausentismo(Listado)', function($excel) use($request){
			$excel->sheet('Ausetismo_Asig', function($sheet) use($request){
				$data = AsigAusentismoModel::join('AusentismoFestivo', 'Calendario_AusenFeriado.id_ausen_fest', '=', 'AusentismoFestivo.id_ausen_fest')
				->whereBetween('Calendario_AusenFeriado.Fecha_inicio', [$request->fechaInicio, $request->fechaFinal])
				->select('RUT', 'Nombre_AusentismoFestivo')
				->selectRaw("DATE_FORMAT(Fecha_inicio,'%d-%m-%Y')")
				->selectRaw("DATE_FORMAT(Fecha_fin,'%d-%m-%Y')")
				->get(); 
				$sheet->fromArray($data);
				$sheet->cell('C1', function($cell) { $cell->setValue('Fecha Inicial'); });
				$sheet->cell('D1', function($cell) { $cell->setValue('Fecha Final'); });
			});
		})->download('xls');
		return back();
	}

	//Función utilísima para comparar fechas dentro e un rango de fechas.
	public static function check_in_range($fecha_inicio, $fecha_fin, $fecha)
	{

		$fecha_inicio = strtotime($fecha_inicio);
		$fecha_fin = strtotime($fecha_fin);
		$fecha = strtotime($fecha);
   
		if(($fecha >= $fecha_inicio) && ($fecha <= $fecha_fin)) 
		{
			return true;
		}
		else
		{
   			return false;
		}
	}
}
