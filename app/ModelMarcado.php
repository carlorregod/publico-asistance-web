<?php namespace Asistance;

use Illuminate\Database\Eloquent\Model;

class ModelMarcado extends Model {

	protected $table='Registro_Asistencia';

	//Debido a que la DB ya está creada, no fue creada con migraciones y por ello, 
	//no se incluirán los timestamps
	public $timestamps = false;

	protected $fillable = ['RUT', 'id_marcado', 'id_celular', 'estado_ausentismo', 'Fecha_marcado', 
	'Hora_marca', 'coordX_GPS', 'coordY_GPS', 'Diferencia_hora', 'contador'];

	protected $primaryKey ='contador';

	protected $foreignKey = ['RUT', 'id_marcado', 'id_celular'];

}
