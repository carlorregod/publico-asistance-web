<?php namespace Asistance;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class AsigHorariosModel extends Model {

	//tabla de relaciones
	protected $table='Usuario_Horario';

	//Debido a que la DB ya está creada, no fue creada con migraciones y por ello, 
	//no se incluirán los timestamps
	public $timestamps = false;

	protected $fillable = ['RUT', 'id_horario', 'Fecha_inicio', 'Fecha_fin', 'contador'];

	protected $dates = ['Fecha_inicio', 'Fecha_fin'];

	protected $primaryKey ='contador';

	protected $foreignKey = ['RUT', 'id_horario'];

}
