<?php namespace Asistance;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class AsigAusentismoModel extends Model {

	//tabla de relaciones
	protected $table='Calendario_AusenFeriado';

    //Debido a que la DB ya está creada, no fue creada con migraciones y por ello, 
    //no se incluirán los timestamps
	public $timestamps = false;

	protected $fillable = ['RUT', 'id_ausen_fest', 'Fecha_inicio', 'Fecha_fin', 'contador'];
	
	protected $dates = ['Fecha_inicio', 'Fecha_fin'];

	protected $primaryKey ='contador';

	protected $foreignKey = ['RUT', 'id_ausen_fest'];

}
