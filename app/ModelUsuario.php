<?php

namespace Asistance;
use Illuminate\Auth\Authenticatable;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
//Para usar los métodos asociados al soft delete y el hash
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;

class ModelUsuario extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, SoftDeletes;

    protected $table='Usuario';

    //Para asegurar el efecto de soft delete
    protected $dates = ['deleted_at'];

    //Debido a que la DB ya está creada, no fue creada con migraciones y por ello, 
    //no se incluirán los timestamps

	public $timestamps = false;

    protected $fillable = ['RUT', 'Apellido_P', 'Apellido_M', 'Nombres', 'Domicilio', 'Telefono', 
                        'Fecha_nacimiento', 'password', 'Rol', 'Fecha_ingreso', 'email'];

    protected $primaryKey = 'RUT';

    protected $hidden = ['password', 'remember_token'];

    public function setPasswordAttribute($pass) 
    {
        $this->attributes['password'] = Hash::make($pass);
    }
    //Desarrollo de las query scopes
    public function scopeRut($query, $RUT)
    {
        if($RUT)
        {
            return $query->where('RUT', 'LIKE', "%$RUT%");
        }
    }
    public function scopeEmail($query, $email)
    {
        if($email)
        {
            return $query->where('email', 'LIKE', "%$email%");
        }
    }
    public function scopeApellido_P($query, $Apellido_P)
    {
        if($Apellido_P)       
        {
            return $query->where('Apellido_P', 'LIKE', "%$Apellido_P%");
        } 
    }
    public function scopeApellido_M($query, $Apellido_M)
    {
        if($Apellido_M)       
        {
            return $query->where('Apellido_M', 'LIKE', "%$Apellido_M%");
        } 
    }
    public function scopeNombres($query, $Nombres)
    {
        if($Nombres)       
        {
            return $query->where('Nombres', 'LIKE', "%$Nombres%");
        } 
    }
}