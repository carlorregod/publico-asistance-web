<?php namespace Asistance;

use Illuminate\Database\Eloquent\Model;

//Para usar los métodos asociados al soft delete
use Illuminate\Database\Eloquent\SoftDeletes;

class ModelHorarios extends Model {

	use SoftDeletes;

    protected $table='Horario';
    //Para asegurar el efecto de soft delete
    protected $dates = ['deleted_at'];

    //Debido a que la DB ya está creada, no fue creada con migraciones y por ello, 
    //no se incluirán los timestamps
	public $timestamps = false;

    protected $fillable = ['id_horario', 'Alias_horario', 'Hora_inicio', 'Hora_final', 'Val_lunes', 'Val_Martes', 
                        'Val_miercoles', 'Val_jueves', 'Val_viernes', 'Val_sabado', 'Val_domingo'];
    
    protected $primaryKey = 'id_horario';

	//Query Scope para la búsqueda de horarios por su alias
	public function scopeAlias_horario($query, $Alias_horario)
    {
        if($Alias_horario)       
        {
            return $query->where('Alias_horario', 'LIKE', "%$Alias_horario%");
        } 
	}
	
}
