<?php namespace Asistance;

use Illuminate\Database\Eloquent\Model;
//Para usar los métodos asociados al soft delete
use Illuminate\Database\Eloquent\SoftDeletes;

class ModelGeolocalizacion extends Model {

	use SoftDeletes;
    
    protected $table='Localizacion_geo';

	//Debido a que la DB ya está creada, no fue creada con migraciones y por ello, 
	//no se incluirán los timestamps
    public $timestamps = false;
    //Para asegurar el efecto de soft delete
    protected $dates = ['deleted_at'];

    protected $fillable = ['id_loc_geo', 'Alias_loc_geo', 'coorX1', 'coorY1', 'coorX2', 'coorY2'];

	protected $primaryKey = 'id_loc_geo';
	
	//Query Scope para la búsqueda de zonas geográficas por su alias
	public function scopeAlias_loc_geo($query, $Alias_loc_geo)
    {
        if($Alias_loc_geo)       
        {
            return $query->where('Alias_loc_geo', 'LIKE', "%$Alias_loc_geo%");
        } 
    }
}
