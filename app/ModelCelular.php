<?php namespace Asistance;

use Illuminate\Database\Eloquent\Model;

class ModelCelular extends Model {

	protected $table='Celular';

	//Debido a que la DB ya está creada, no fue creada con migraciones y por ello, 
	//no se incluirán los timestamps
	public $timestamps = false;
	
	protected $fillable = ['id_celular', 'Numero_celular', 'Compania', 'IMEI'];

	protected $primaryKey = 'id_celular';

	//Query Scope para la búsqueda de celulares
	public function scopeNumero_celular($query, $Numero_celular)
    {
        if($Numero_celular)       
        {
            return $query->where('Numero_celular', 'LIKE', "%$Numero_celular%");
        } 
    }

}
