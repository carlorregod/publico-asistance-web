<?php namespace Asistance;

use Illuminate\Database\Eloquent\Model;
//Para usar los métodos asociados al soft delete
use Illuminate\Database\Eloquent\SoftDeletes;

class ModelAusentismo extends Model {

	use SoftDeletes;

    protected $table='AusentismoFestivo';
    //Para asegurar el efecto de soft delete
    protected $dates = ['deleted_at'];

    //Debido a que la DB ya está creada, no fue creada con migraciones y por ello, 
    //no se incluirán los timestamps
	public $timestamps = false;

    protected $fillable = ['id_ausen_fest', 'Nombre_AusentismoFestivo', 'pago'];

    protected $primaryKey = 'id_ausen_fest';

	//Query Scope para la búsqueda de celulares
	public function scopeNombre_AusentismoFestivo($query, $Nombre_AusentismoFestivo)
    {
        if($Nombre_AusentismoFestivo)       
        {
            return $query->where('Nombre_AusentismoFestivo', 'LIKE', "%$Nombre_AusentismoFestivo%");
        } 
    }
}
