<!DOCTYPE HTML>
<!--
    Hyperspace by HTML5 UP
    html5up.net | @ajlkn
    Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
    <head>
        <title>AsisTance</title>
        <meta charset="utf-8" />
	<meta name="google-site-verification" content="4g3tF0DqSY6Bea54O1BuzMXSplDmvHID9wCtM4GZ7lk" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="{{ URL::asset('assets/css/main.css')}}" />
        <noscript><link rel="stylesheet" href="{{ URL::asset('assets/css/noscript.css')}}" /></noscript>
    </head>
    <body class="is-preload">
        <!-- Sidebar -->
            <section id="sidebar">
                <div class="inner">
                    <nav>
                        <ul>
                                <li><a href="{{ URL::asset('main')}}"><img src="{{ URL::asset('images/clockc.png')}}" alt="Logo"></a></li>
                            @yield('submenu')
                        </ul>
                    </nav>
                </div>
            </section>

        <!-- Wrapper -->
            <div id="wrapper">

                <!-- Intro -->
                    <section id="intro" class="wrapper style1 fullscreen fade-up">
                        <div class="inner">
                                <h4 style="color:white;">Bienvenido(a), {!! Auth::user()->Nombres !!} {!! Auth::user()->Apellido_P !!} {!! Auth::user()->Apellido_M !!} RUT: {!! Auth::user()->RUT !!}</h4>
                            @yield('bienvenida')              
                        </div>
                    </section>
                <!-- Uno. Hay hasta 3 subcategorías-->
                    <section id="one" class="wrapper style2 spotlights">
                        <section>
                            <div class="content">
                                <div class="inner">
                                    @yield('subseccion11')
                                </div>
                            </div>
                        </section>
                                    @yield('subseccion12')
                                    @yield('subseccion13')
                    </section>
            </div>

        <!-- Footer -->
            <footer id="footer" class="wrapper style1-alt">
                <div class="inner">
                    <ul class="menu">
                        <li>&copy; AsisTance. Todos los derechos reservados.</li><li></li>
                    </ul>
                </div>
            </footer>

        <!-- Scripts -->
            <script src="{{ URL::asset('assets/js/validador.js')}}"></script>      
            <script src="{{ URL::asset('assets/js/jquery.min.js')}}"></script>
            <script src="{{ URL::asset('assets/js/jquery.scrollex.min.js')}}"></script>
            <script src="{{ URL::asset('assets/js/jquery.scrolly.min.js')}}"></script>
            <script src="{{ URL::asset('assets/js/browser.min.js')}}"></script>
            <script src="{{ URL::asset('assets/js/breakpoints.min.js')}}"></script>
            <script src="{{ URL::asset('assets/js/util.js')}}"></script>
            <script src="{{ URL::asset('assets/js/main.js')}}"></script>
    </body>
</html>
