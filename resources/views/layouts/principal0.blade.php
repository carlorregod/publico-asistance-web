<!DOCTYPE HTML>
<!--
    Hyperspace by HTML5 UP
    html5up.net | @ajlkn
    Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
    <head>
        <title>AsisTance</title>
        <meta charset="utf-8" />
	<meta name="google-site-verification" content="4g3tF0DqSY6Bea54O1BuzMXSplDmvHID9wCtM4GZ7lk" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        {!!Html::style('assets/css/main.css')!!}
        <noscript>{!!Html::style('assets/css/noscript.css')!!}</noscript>
    </head>
    <body class="is-preload">

        <!-- Wrapper -->
            <div id="wrapper">

                <!-- Intro -->
                    <section id="intro" class="wrapper style1 fullscreen fade-up">
                        <div class="inner">
                            @yield('bienvenida')              
                        </div>
                    </section>
             </div>

        <!-- Footer -->
            <footer id="footer" class="wrapper style1-alt">
                <div class="inner">
                    <ul class="menu">
                        <li>&copy; AsisTance. Todos los derechos reservados.</li><li></li>
                    </ul>
                </div>
            </footer>

        <!-- Scripts -->
	    {!!Html::script('assets/js/validador.js')!!}
        {!!Html::script('assets/js/jquery.min.js')!!}
        {!!Html::script('assets/js/jquery.scrollex.min.js')!!}
        {!!Html::script('assets/js/jquery.scrolly.min.js')!!}
        {!!Html::script('assets/js/browser.min.js')!!}
        {!!Html::script('assets/js/breakpoints.min.js')!!}
        {!!Html::script('assets/js/util.js')!!}
        {!!Html::script('assets/js/main.js')!!}
    </body>
</html>
