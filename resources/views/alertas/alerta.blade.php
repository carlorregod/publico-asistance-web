@if(Session::has('message'))
<div class="alert alert-success" role="alert">
<h4 class="alert-heading">{{Session::get('message')}}</h4>
</div>
@endif
