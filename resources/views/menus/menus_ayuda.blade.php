<h2>Menú Auxiliar</h2>
<p>Por favor, selecciona opción</p>
<ul class="actions">
    <li><a href="{{ URL::asset('main')}}" class="button submit">Regresar</a></li>
    <li><a href="{{ URL::asset('main/cambiapw')}}" class="button submit">Cambiar Contraseña</a></li>
    <li><a href="{{ URL::asset('#')}}" class="button submit">Ayuda</a></li>
    <li><a href="{{ URL::asset('logout')}}" class="button submit">Salir</a></li>
</ul>