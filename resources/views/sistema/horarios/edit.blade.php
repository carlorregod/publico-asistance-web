@extends('layouts.principal')

@section('bienvenida')
    <h1>Horario</h1>
    <p>Bienvenidos a Asistance</p>
    <p>Administración de horarios. Efectúe su edición</p>
    <!-- Formulario de edición -->
    @include('alertas.alerta')
	@include('alertas.validaformulario')
    {!! Form::model($horario, ['route' => ['main.horarios.administrar.update', $horario->id_horario], 'method' => 'PUT']) !!}
    <div class="fields">
            <div class="field">
                {!! Form::label('Alias_horario', 'Alias del horario a actualizar: ') !!}
                {!! Form::text('Alias_horario', null, ['class' => 'form-control', 'readonly'=>'readonly']) !!}
            </div>
            <div class="field half">
                {!! Form::label('Hora_inicio', 'Hora inicio bloque: ') !!}
                <input type="time" name="Hora_inicio" value="00:00:00" step="1">
            </div>
            <div class="field half">
                {!! Form::label('Hora_final', 'Hora fin bloque: ') !!}
                <input type="time" name="Hora_final" value="00:00:00" step="1">
            </div>
            <div class="field quarter">
                <input id="Val_lunes" name="Val_lunes[]" type="checkbox" class="checkthis" value/><label for="Val_lunes">Lunes</label>
            </div>
            <div class="field quarter">
                <input id="Val_Martes" name="Val_Martes[]" type="checkbox" class="checkthis"/><label for="Val_Martes">Martes</label>
            </div>
            <div class="field quarter">
                <input id="Val_miercoles" name="Val_miercoles[]" type="checkbox" class="checkthis"/><label for="Val_miercoles">Miércoles</label>
            </div>
            <div class="field quarter">
                <input id="Val_jueves" name="Val_jueves[]" type="checkbox" class="checkthis"/><label for="Val_jueves">Jueves</label>
            </div>
            <div class="field quarter">
                <input id="Val_viernes" name="Val_viernes[]" type="checkbox" class="checkthis"/><label for="Val_viernes">Viernes</label>
            </div>
            <div class="field quarter">
                <input id="Val_sabado" name="Val_sabado[]" type="checkbox" class="checkthis"/><label for="Val_sabado">Sábado</label>
            </div>
            <div class="field quarter">
                <input id="Val_domingo" name="Val_domingo[]" type="checkbox" class="checkthis"/><label for="Val_domingo">Domingo</label>
            </div>
        </div>
    <ul class="actions">
    <li>
    {!! Form::submit('Actualizar',['class' => 'btn btn-primary']) !!}
    </li>
    </ul>
    {!! Form::close() !!}
    <!-- Botón de elimnación de registros -->
    {!! Form::open(['route' => ['main.horarios.administrar.destroy', $horario->id_horario], 'method' => 'DELETE']) !!}
        <ul class="actions">
    <li>{!! Form::submit('Eliminar',['class' => 'btn btn-danger']) !!}</li>
        </ul>
    {!! Form::close() !!}
@stop            

@section('submenu')
@include('menus.horarios')
@stop            

@section('subseccion11')
@include('menus.menus_ayuda')
@stop    