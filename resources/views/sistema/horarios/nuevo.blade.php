@extends('layouts.principal')

@section('bienvenida')
    <h1>Horario</h1>
    <p>Bienvenidos a Asistance</p>
	<p>Por favor, complete el formulario.</p>
		
		@include('alertas.alerta')
		@include('alertas.validaformulario')
	<section>
	<!--Formulario para crear un nuevo horario y ser almacenado a la BBDD -->
	{!! Form::open(['route' => 'main.horarios.administrar.store', 'method' => 'POST']) !!}
		<div class="fields">
			<div class="field">
				{!! Form::label('Alias_horario', 'Alias o pseudónimo: ') !!}
				{!! Form::text('Alias_horario', null, ['class' => 'form-control', 'maxlength' => '24', 'placeholder' =>'Ejemplo: HORARIO-0001']) !!}
			</div>
			<div class="field half">
				{!! Form::label('Hora_inicio', 'Hora inicio bloque: ') !!}
				<input type="time" name="Hora_inicio" value="00:00:00" step="1">
			</div>
			<div class="field half">
				{!! Form::label('Hora_final', 'Hora fin bloque: ') !!}
				<input type="time" name="Hora_final" value="00:00:00" step="1">
			</div>
			<div class="field quarter">
				<input id="Val_lunes" name="Val_lunes[]" type="checkbox" class="checkthis" value/><label for="Val_lunes">Lunes</label>
			</div>
			<div class="field quarter">
				<input id="Val_Martes" name="Val_Martes[]" type="checkbox" class="checkthis"/><label for="Val_Martes">Martes</label>
			</div>
			<div class="field quarter">
				<input id="Val_miercoles" name="Val_miercoles[]" type="checkbox" class="checkthis"/><label for="Val_miercoles">Miércoles</label>
			</div>
			<div class="field quarter">
				<input id="Val_jueves" name="Val_jueves[]" type="checkbox" class="checkthis"/><label for="Val_jueves">Jueves</label>
			</div>
			<div class="field quarter">
				<input id="Val_viernes" name="Val_viernes[]" type="checkbox" class="checkthis"/><label for="Val_viernes">Viernes</label>
			</div>
			<div class="field quarter">
				<input id="Val_sabado" name="Val_sabado[]" type="checkbox" class="checkthis"/><label for="Val_sabado">Sábado</label>
			</div>
			<div class="field quarter">
				<input id="Val_domingo" name="Val_domingo[]" type="checkbox" class="checkthis"/><label for="Val_domingo">Domingo</label>
			</div>
		</div>
		<ul class="actions">
			<li>
			{!! Form::submit('Generar nuevo Horario',['class' => 'btn btn-primary']) !!}
			</li>
		</ul>
	{!! Form::close() !!}
		</form>
	</section>
@stop       

@section('submenu')
@include('menus.horarios')
@stop           

@section('subseccion11')
@include('menus.menus_ayuda')
@stop       