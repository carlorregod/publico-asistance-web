@extends('layouts.principal')

@section('bienvenida')
    <h1>Horario</h1>
    <p>Bienvenidos a Asistance</p>
	<p>Nueva asignación de horario. Favor complete formulario</p>
	@include('alertas.alerta')
	@include('alertas.validaformulario')
	<section>
		<!--Formulario para crear una nueva asociación de horarios y ser almacenado a la BBDD -->
		{!! Form::open(['route' => 'main.horarios.asigadministrar.store', 'method' => 'POST']) !!}
			<div class="fields">
				<div class="field half">
					{!! Form::label('RUT', 'Seleccionar RUT del trabajador: ') !!}
                    <select name="RUT" id="RUT" class="form-control" required="required">
						@foreach($usuarios as $usuario)
							<option value="{{ $usuario->RUT}}" id="{{ $usuario->RUT}}" name="{{ $usuario->RUT}}">{{ $usuario->RUT}}: {{ $usuario->Apellido_P}} {{ $usuario->Apellido_M}} {{ $usuario->Nombres }}</option>
						@endforeach
					</select>
				</div>
				<div class="field half">
					{!! Form::label('Horario', 'Seleccione alias horario: ') !!}
					<select name="id_horario" id="id_horario" class="form-control" required="required">
						@foreach($horarios as $horario)
							<option value="{{ $horario->id_horario}}" id="{{ $horario->id_horario}}" name="{{ $horario->id_horario}}">{{ $horario->Alias_horario }}</option>
						@endforeach
					</select>
                </div>
                <div class="field half">
                    {!! Form::label('Fecha_inicio', 'Especifique fecha inicio del bloque horario: ') !!}
                    {!! Form::date('Fecha_inicio', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
                    </div>
                <div class="field half">
                    {!! Form::label('Fecha_fin', 'Seleccionar fecha final del bloque horario: ') !!}
                    {!! Form::date('Fecha_fin', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
                </div>
			</div>
			<ul class="actions">
				<li>
				{!! Form::submit('Generar nueva asociación',['class' => 'btn btn-primary']) !!}
				</li>
			</ul>
		{!! Form::close() !!}
	</section>	
@stop            

@section('submenu')
@include('menus.horarios')
@stop           

@section('subseccion11')
@include('menus.menus_ayuda')
@stop       