@extends('layouts.principal')

@section('bienvenida')
    <h1>Horario</h1>
    <p>Bienvenidos a Asistance</p>
    <p>Administración de asignación de horarios. Efectúe su edición</p>
    @include('alertas.alerta')
	@include('alertas.validaformulario')
    {!! Form::model($asighorario, ['route' => ['main.horarios.asigadministrar.update', $asighorario->contador], 'method' => 'PUT']) !!}
    <div class="fields">
        <div class="field half">
            {!! Form::label('RUT', 'RUT del trabajador: ') !!}
            {!! Form::text('RUT', null, ['class' => 'form-control', 'readonly'=>'readonly']) !!}
        </div>
        <div class="field half">
            {!! Form::label('Horario', 'Seleccione alias horario: ') !!}
            <select name="id_horario" id="id_horario" class="form-control" required="required">
                @foreach($horarios as $horario)
                    <option value="{{ $horario->id_horario}}" id="{{ $horario->id_horario}}" name="{{ $horario->id_horario}}">{{ $horario->Alias_horario }}</option>
                @endforeach
            </select>
        </div>
        <div class="field half">
            {!! Form::label('Fecha_inicio', 'Especifique fecha inicio del ausentismo: ') !!}
            {!! Form::date('Fecha_inicio', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
            </div>
        <div class="field half">
            {!! Form::label('Fecha_fin', 'Seleccionar fecha final del ausentismo: ') !!}
            {!! Form::date('Fecha_fin', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
        </div>
    </div>
    <ul class="actions">
    <li>
    {!! Form::submit('Actualizar',['class' => 'btn btn-primary']) !!}
    </li>
    </ul>
    {!! Form::close() !!}

    {!! Form::open(['route' => ['main.horarios.asigadministrar.destroy', $asighorario->contador], 'method' => 'DELETE']) !!}
        <ul class="actions">
    <li>{!! Form::submit('Eliminar',['class' => 'btn btn-danger']) !!}</li>
        </ul>
    {!! Form::close() !!}
@stop            

@section('submenu')
@include('menus.horarios')
@stop           

@section('subseccion11')
@include('menus.menus_ayuda')
@stop    