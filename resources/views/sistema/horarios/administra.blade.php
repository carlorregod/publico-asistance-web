@extends('layouts.principal')

@section('bienvenida')
    <h1>Horario</h1>
    <p>Bienvenidos a Asistance</p>
    <p>Administración de horarios. Especificar operación</p>
    
	@include('alertas.alerta')
	@include('alertas.validaformulario')

    <!-- Buscar -->
    {!! Form::open(['route' => 'main.horarios.administrar.show', 'id'=>'cero','method' => 'GET'])!!}
    <div class="fields">
		<div class="field half">
        {!! Form::label('Alias_horario', 'Buscar por alias de horario: ') !!}
        {!! Form::text('Alias_horario', null, ['class' => 'form-control', 'maxlength' => '24', 'placeholder' =>'Ejemplo: HORARIO-0001']) !!}       
        </div>
	</div> 
    <p></p>
    {!! Form::submit('Mostrar resultados',['class' => 'button small']) !!}
    {!! Form::close()!!}
    <!-- Exportar Excel -->
    <a href="{{ url('main/horarios/export')}}" class="button submit">Exporta todos los Horarios *xls</a>
    <p></p>
    <!-- Tabla -->
    <h2>Tabla de Horarios</h2>
        {!! Form::open(['url' => 'main/horarios/administrar/borra', 'id'=>'uno','method' => 'post']) !!}        
        <p></p>
		<!--Tabla de ocurrencias de horarios -->
        <table class="table" size="small">
                <thead>
                    <th>Operación</th>
                    <th>Check</th>
                    <th>Alias Horario</th>
                    <th>Hora <br/>Inicial</th>
                    <th>Hora <br/>Final</th>
                    <th>L</th>
                    <th>M</th>
                    <th>Mi</th>
                    <th>J</th>
                    <th>V</th>
                    <th>S</th>
                    <th>D</th>
                </thead>
            @foreach($horarios as $horario) 
                <tbody>
                    <td>{!! link_to_route('main.horarios.administrar.edit', $title = 'Modificar', $parameters = $horario->id_horario , $attributes = ['class'=>'btn btn-primary']) !!}</td>
                    <td align="center"><input id="{{$horario->id_horario}}" value="{{$horario->id_horario}}" name="delete[]" type="checkbox" class="checkthis"/><label for="{{$horario->id_horario}}"></label></td>
                    <td>{{$horario->Alias_horario}}</td>
                    <td>{{$horario->Hora_inicio}}</td>
                    <td>{{$horario->Hora_final}}</td>
                    <td>                    
                        @if($horario->Val_lunes === 0)
                        No
                        @else
                        Sí     
                        @endif
                    </td>
                    <td>                    
                        @if($horario->Val_Martes === 0)
                        No
                        @else
                        Sí     
                        @endif
                    </td>
                    <td>                    
                        @if($horario->Val_miercoles === 0)
                        No
                        @else
                        Sí     
                        @endif
                    </td>
                    <td>                    
                        @if($horario->Val_jueves === 0)
                        No
                        @else
                        Sí     
                        @endif
                    </td>
                    <td>                    
                        @if($horario->Val_viernes === 0)
                        No
                        @else
                        Sí     
                        @endif
                    </td>
                    <td>                    
                        @if($horario->Val_sabado === 0)
                        No
                        @else
                        Sí     
                        @endif
                    </td>
                    <td>                    
                        @if($horario->Val_domingo === 0)
                        No
                        @else
                        Sí     
                        @endif
                    </td>
                </tbody>
            @endforeach
        </table>	
    {!! $horarios->render()!!}
    <p></p>
    <!-- Eliminación masiva -->
    {!! Form::submit('Eliminar check',['class' => 'button small']) !!}
    {!! Form::close()!!}
@stop            

@section('submenu')
@include('menus.horarios')
@stop           

@section('subseccion11')
@include('menus.menus_ayuda')
@stop    