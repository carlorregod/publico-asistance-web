@extends('layouts.principal')

@section('bienvenida')
    <h1>Horario</h1>
    <p>Bienvenidos a Asistance</p>
    <p>Administración de asignación de horarios a trabajadores</p>
    
	@include('alertas.alerta')
	@include('alertas.validaformulario')
        <!-- Buscar -->
        <h4>Filtros de visualización (rangos respecto a fecha iniciales de horario)</h4>
        {!! Form::open(['route' => 'main.horarios.asigadministrar.show', 'method' => 'GET'])!!}
        <div class="fields">
            <div class="field half">
                {!! Form::label('RUT', 'Buscar por RUT: ') !!}
                <select name="RUT" id="RUT" class="form-control" required="required">
                    @foreach($usuarios as $usuario)
                        <option value="{{ $usuario->RUT}}" id="{{ $usuario->RUT}}" name="{{ $usuario->RUT}}">{{ $usuario->RUT}}: {{ $usuario->Apellido_P}} {{ $usuario->Apellido_M}} {{ $usuario->Nombres }}</option>
                    @endforeach
                </select>
            </div>
            <div class="field quarter">    
                {!! Form::label('fechaInicio', 'Buscar por fecha:  I') !!}         	
                {!! Form::date('fechaInicio', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
            </div>
            <div class="field quarter">    
                {!! Form::label('fechaFinal', 'Buscar por fecha: F ') !!}         	
                {!! Form::date('fechaFinal', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
            </div>
        </div>
        <p></p>
        {!! Form::submit('Mostrar resultados',['class' => 'button small']) !!}
        {!! Form::close()!!}
        <h4></h4>
        <!-- Exportar Excel -->
        <h4>Filtros de exportación tabla (rangos respecto a fecha iniciales de horario)</h4>
        {!! Form::open(['url' => 'main/horarios/asigexport', 'method' => 'GET']) !!}
        <div class="fields">
            <div class="field quarter">    
                {!! Form::label('fechaInicio', 'Fecha inicio: ') !!}         	
                {!! Form::date('fechaInicio', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
            </div>
            <div class="field quarter">    
                {!! Form::label('fechaFinal', 'Fecha fin: ') !!}         	
                {!! Form::date('fechaFinal', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
            </div>
        </div>
        <p></p>
        {!! Form::submit('Exporta todas las asignaciones de horarios *xls',['class' => 'button']) !!}
        {!! Form::close()!!}
        <p></p>
        <!-- Tabla -->
        <h2>Tabla de Asignación de Horarios</h2>        
        {!! Form::open(['url' => 'main/horarios/asigadministrar/borra', 'method' => 'post']) !!}              
        <p></p>
		<!--Tabla de ocurrencias de horarios -->
        <table class="table" align="center" id="tblData">
                <thead>
                    <th>Operación</th>
                    <th align="center">Check</th>
                    <th>RUT</th>
                    <th>Alias  <br/>horario</th>
                    <th>Fecha Inicial</th>
                    <th>Fecha Final</th>
                </thead>
            @foreach($asighorarios as $asighorario) 
                <tbody>
                    <td>{!! link_to_route('main.horarios.asigadministrar.edit', $title = 'Modificar', $parameters = $asighorario->contador, $attributes = ['class'=>'btn btn-primary']) !!}</td>
                    <td align="center"><input id="{{$asighorario->contador}}" value="{{$asighorario->contador}}" name="delete[]" type="checkbox" class="checkthis"/><label for="{{$asighorario->contador}}"></label></td>
                    <td>{{$asighorario->RUT}}</td>
                    <td>{{$asighorario->Alias_horario}}</td>
                    <td>{{ date('d-m-Y',strtotime($asighorario->Fecha_inicio)) }}</td>
                    <td>{{ date('d-m-Y',strtotime($asighorario->Fecha_fin))}}</td>
                </tbody>
            @endforeach
        </table>
    {!! $asighorarios->render()!!}	
    <p></p>
    <!-- Eliminación masiva -->
    {!! Form::submit('Eliminar check de asociaciones',['class' => 'button small']) !!}  
    {!! Form::close() !!}	

    
@stop            

@section('submenu')
@include('menus.horarios')
@stop             

@section('subseccion11')
@include('menus.menus_ayuda')
@stop    