@extends('layouts.principal0')

<?php $message=Session::get('message')?>

@section('bienvenida')
<h1>AsisTance</h1>
<p>Bienvenidos a Asistance.</p>
<p>Por favor, ingrese sus credenciales de acceso al sistema.</p>
@include('alertas.errores')
@include('alertas.validaformulario')
	<section>
		{!! Form::open(['route' => 'login.store', 'method' => 'POST']) !!}
			<div class="fields">
				<div class="field half">
					{!! Form::label('rut', 'Ingrese su RUT. Sin puntos ni guiones. Si el RUT termina en K, reemplazarlo por 0: ') !!}
					{!! Form::text('RUT', null, ['class' => 'form-control', 'onkeypress' => 'return soloNumeroNatural(event)', 'maxlength' => '10', 'placeholder' =>'Ejemplo: 123452157']) !!}
				</div>
				<div class="field half">
					{!! Form::label('Contrasena', 'Ingrese contraseña: ') !!}<br/>
					{!! Form::password('password', null, ['class' => 'form-control']) !!}
				</div>
			</div>
			<ul class="actions">
				<li>
					{!! Form::submit('Ingresar',['class' => 'btn btn-primary']) !!}
				</li>
			</ul>
		{!! Form::close() !!}
	</section>
@stop            
