@extends('layouts.principal')

@section('bienvenida')
    <h1>Opciones de Marcado</h1>
    <p>Bienvenidos a Asistance</p>
	<p>Realizar la marca de urgencia.</p>
		
		@include('alertas.alerta')
		@include('alertas.validaformulario')
	<section>
		<!--Formulario para crear una nueva marca y ser almacenado a la BBDD -->
		{!! Form::open(['route' => 'main.marcado.administrar.store', 'method' => 'POST']) !!}
				<div class="fields">
					<div class="field">
						{!! Form::label('RUT', 'Seleccionar RUT del trabajador: ') !!}
						<select name="RUT" id="RUT" class="form-control" required="required">
							@foreach($usuarios as $usuario)
								<option value="{{ $usuario->RUT}}" id="{{ $usuario->RUT}}" name="{{ $usuario->RUT}}">{{ $usuario->RUT}}: {{ $usuario->Apellido_P}} {{ $usuario->Apellido_M}} {{ $usuario->Nombres }}</option>
							@endforeach
						</select>
                    </div>
					<div class="field half">
						{!! Form::label('Marcado', 'Seleccione tipo de marcado: ') !!}
						<select name="id_marcado" id="id_marcado" class="form-control" required="required">
							@foreach($marcados as $marcado)
								<option value="{{ $marcado->id_marcado}}" id="{{ $marcado->id_marcado}}" name="{{ $marcado->id_marcado}}">{{ $marcado->Tipo_marca }}</option>
							@endforeach
						</select>
					</div>
					<div class="field half">
						{!! Form::label('Celular', 'Seleccione celular: ') !!}
						<select name="id_celular" id="id_celular" class="form-control" required="required">
							@foreach($celulars as $celular)
								<option value="{{ $celular->id_celular}}" id="{{ $celular->id_celular}}" name="{{ $celular->id_celular}}">{{ $celular->Numero_celular }}</option>
							@endforeach
						</select>
					</div>
					<div class="field half">
						{!! Form::label('Fecha_marcado', 'Especifique fecha marcado: ') !!}
						{!! Form::date('Fecha_marcado', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
					</div>
					<div class="field half">
						{!! Form::label('Hora', 'Hora de la marca: ') !!}
						<input type="time" name="Hora_marca" value="00:00:00" step="1">
					</div>
					<h4>Arrastre el marcador del mapa para captuar el punto geográfica a establecer la marca</h4><br />
					<h4>No olvide aceptar que Google permita detectar su posición actual. </h4><br />
					<div class="field" id="map"style="width: 800px; height:600px;"><br /></div>
					<div class="field">
						{!! Form::label('coordX_GPS', 'Componente X coordenada de marca: ') !!}
						<input type="text" name="coordX_GPS" id="long" readonly="readonly" />
						{!! Form::label('coordY_GPS', 'Componente Y primer vértice superior izquierdo: ') !!}
						<input type="text" name="coordY_GPS" id="lat" readonly="readonly" />
					</div>
				</div>
                <ul class="actions">
					<li><a href="" class="button submit">Generar marca de emergencia</a></li>
                </ul>
			</form>
		</section>
		<script src="{{ URL::asset('assets/js/locgeo1.js')}}"></script>
@stop          

@section('submenu')
@include('menus.marcado')
@stop              

@section('subseccion11')
@include('menus.menus_ayuda')
<!-- La clave de API acá la obtuve de cuenta Google Cloud: AIzaSyBz2P8cnKkLgEjK2h2C1dtfD8B8gRmEARI --> 
<script async defer src="https://maps.googleapis.com/maps/api/js?key=
AIzaSyBz2P8cnKkLgEjK2h2C1dtfD8B8gRmEARI&callback=initMap"
type="text/javascript"></script>
@stop  