@extends('layouts.principal')

@section('bienvenida')
    <h1>Opciones de Marcado</h1>
    <p>Bienvenidos a Asistance</p>
    <p>Visualzación de marcas de asistencia</p>
    
    @include('alertas.alerta')
    @include('alertas.validaformulario')
    
        <!-- Buscar -->
        <h4>Filtros de visualización</h4>
        {!! Form::open(['route' => 'main.marcado.administrar.show', 'method' => 'GET'])!!}
        <div class="fields">
            <div class="field">
                {!! Form::label('RUT', 'Buscar por RUT: ') !!}
                <select name="RUT" id="RUT" class="form-control" required="required">
                    @foreach($usuarios as $usuario)
                        <option value="{{ $usuario->RUT}}" id="{{ $usuario->RUT}}" name="{{ $usuario->RUT}}">{{ $usuario->RUT}}: {{ $usuario->Apellido_P}} {{ $usuario->Apellido_M}} {{ $usuario->Nombres }}</option>
                    @endforeach
                </select>
            </div>
            <div class="field quarter">    
                {!! Form::label('fechaInicio', 'Fecha inicio: ') !!}         	
                {!! Form::date('fechaInicio', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
            </div>
            <div class="field quarter">    
                {!! Form::label('fechaFinal', 'Fecha fin: ') !!}         	
                {!! Form::date('fechaFinal', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
            </div>
        </div>
        <p></p>
        {!! Form::submit('Mostrar resultados',['class' => 'button small']) !!}
        {!! Form::close()!!}
        <h4></h4>
        <!-- Exportar Excel -->
        <h4>Filtros de exportación tabla</h4>
        {!! Form::open(['url' => 'main/marcado/export', 'method' => 'GET']) !!}
        <div class="fields">
            <div class="field quarter">    
                {!! Form::label('fechaInicio', 'Fecha inicio: ') !!}         	
                {!! Form::date('fechaInicio', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
            </div>
            <div class="field quarter">    
                {!! Form::label('fechaFinal', 'Fecha fin: ') !!}         	
                {!! Form::date('fechaFinal', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
            </div>
        </div>
        <p></p>
        {!! Form::submit('Exporta todas las marcas de asistencia *xls',['class' => 'button']) !!}
        {!! Form::close()!!}
        <p></p>
        <!-- Tabla -->
        <h2>Tabla de Marcas de Asistencia</h2>
        {!! Form::open(['url' => 'main/marcado/administrar/borra', 'method' => 'POST']) !!}
		<!--Tabla de ocurrencias de marcas -->
        <table class="table" align="center" size="small" id="tblData">
                <thead>
                    <th align="center">Check</th>
                    <th>RUT y nombre</th>
                    <th>Tipo marca</th>
                    <th>Celular</th>
                    <th>Fecha Marca</th>
                    <th>Hora Marca</th>
                    <th>Coordenada <br/>Marca (X,Y)</th>
                </thead>
            @foreach($marcasasistencias as $marcasasistencia) 
                <tbody>
                    <td align="center"><input id="{{$marcasasistencia->contador}}" value="{{$marcasasistencia->contador}}" name="delete[]" type="checkbox" class="checkthis"/><label for="{{$marcasasistencia->contador}}"></label></td>
                    <td>{{$marcasasistencia->RUT}} - {{$marcasasistencia->Apellido_P}} {{$marcasasistencia->Apellido_M}} {{$marcasasistencia->Nombres}}</td>
                    <td>{{ $marcasasistencia->Tipo_marca }}</td>
                    <td>{{$marcasasistencia->Numero_celular}}</td>
                    <td>{{date('d-m-Y',strtotime($marcasasistencia->Fecha_marcado))}}</td>
                    <td>{{$marcasasistencia->Hora_marca}}</td>
                    <td>({{$marcasasistencia->coordX_GPS}} , {{$marcasasistencia->coordY_GPS}})</td>
                </tbody>
            @endforeach
        </table>	
        {!! $marcasasistencias->render()!!}
        <p></p>
        <!-- Eliminación masiva -->
        <h3>Advertencia: Si borra alguno de estos registros, éstos no podrán ser recuperados o restaurados. Manipule esta opción bajo su responsabilidad.</h3>
        {!! Form::submit('Eliminar marcas de check',['class' => 'button small']) !!}
        {!!Form::close()!!}

        
@stop            

@section('submenu')
@include('menus.marcado')
@stop      

@section('subseccion11')
@include('menus.menus_ayuda')
@stop  