@extends('layouts.principal')

@section('bienvenida')
    <h1>Zonas Geográficas</h1>
    <p>Bienvenidos a Asistance</p>
    <p>Por favor, seleccione opción</p>
    <ul class="actions">
        <li><a href="{{ URL::asset('main/zonageo/nuevo')}}" class="button submit">Nueva Zona Geográfica</a></li>
    </ul>
    <ul class="actions">
        <li><a href="{{ URL::asset('main/zonageo/administrar')}}" class="button submit">Administrar Zonas Geográficas</a></li>
    </ul>
    <ul class="actions">
        <li><a href="{{ URL::asset('main/zonageo/asignuevo')}}" class="button submit">Asignar Zona Geográfica</a></li>
    </ul>
    <ul class="actions">
        <li><a href="{{ URL::asset('main/zonageo/asigadministrar')}}" class="button submit">Administrar Asignaciones</a></li>
    </ul>

@stop          

@section('submenu')
@include('menus.main')
@stop            

@section('subseccion11')
@include('menus.menus_ayuda')
@stop   