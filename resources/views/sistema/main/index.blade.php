@extends('layouts.principal')

@section('bienvenida')
    <h1>AsisTance</h1>
    <p>Bienvenidos a Asistance.</p>
    <ul class="actions">
        <li><a href="#one" class="button scrolly">Menú Principal</a></li>
    </ul>
@stop          

@section('submenu')
@include('menus.main')
@stop            

@section('subseccion11')
    <h2>Menú Principal</h2>
    <p>Por favor, selecciona opción</p>
    <ul class="actions">
        <li><a href="{{ URL::asset('main/usuarios')}}" class="button submit">Usuarios</a></li>
        <li><a href="{{ URL::asset('main/horarios')}}" class="button submit">Horario</a></li>
        <li><a href="{{ URL::asset('main/zonageo')}}" class="button submit">Zona geográfica</a></li>
    </ul>
    <ul class="actions">
        <li><a href="{{ URL::asset('main/marcado')}}" class="button submit">Marcas de Asistencia</a></li>
        <li><a href="{{ URL::asset('main/ausentismo')}}" class="button submit">Ausentismos</a></li>
		<li><a href="{{ URL::asset('main/cambiapw')}}" class="button submit">Cambiar Contraseña</a></li>
    </ul>
    <ul class="actions">
        <li><a href="{{ URL::asset('logout')}}" class="button submit">Salir del sistema</a></li>
    </ul>
@stop              
