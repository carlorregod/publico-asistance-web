@extends('layouts.principal')

@section('bienvenida')
    <h1>Horarios</h1>
    <p>Bienvenidos a Asistance</p>
    <p>Por favor, seleccione opción</p>
    <ul class="actions">
        <li><a href="{{ URL::asset('main/horarios/nuevo')}}" class="button submit">Nuevo Horario</a></li>
    </ul>
    <ul class="actions">
            <li><a href="{{ URL::asset('main/horarios/administrar')}}" class="button submit">Administrar Horarios</a></li>
        </ul>
    <ul class="actions">
        <li><a href="{{ URL::asset('main/horarios/asignuevo')}}" class="button submit">Asignar Horarios</a></li>
    </ul>
    <ul class="actions">
        <li><a href="{{ URL::asset('main/horarios/asigadministrar')}}" class="button submit">Administrar Asignaciones</a></li>
    </ul>
@stop            

@section('submenu')
@include('menus.main')
@stop              

@section('subseccion11')
@include('menus.menus_ayuda')
@stop    