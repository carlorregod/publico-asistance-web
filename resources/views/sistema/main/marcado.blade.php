@extends('layouts.principal')

@section('bienvenida')
    <h1>Marcas de Asistencia</h1>
    <p>Bienvenidos a Asistance</p>
    <p>Por favor, seleccione opción</p>
    <ul class="actions">
        <li><a href="{{ URL::asset('main/marcado/marcau')}}" class="button submit">Marca de asistencia de Urgencia</a></li>
    </ul>
    <ul class="actions">
        <li><a href="{{ URL::asset('main/marcado/administrar')}}" class="button submit">Administrar Marcas de asistencia</a></li>
    </ul>
@stop         

@section('submenu')
@include('menus.main')
@stop          

@section('subseccion11')
@include('menus.menus_ayuda')
@stop 