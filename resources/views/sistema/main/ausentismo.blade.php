@extends('layouts.principal')

@section('bienvenida')
    <h1>Ausentismos</h1>
    <p>Bienvenidos a Asistance</p>
    <p>Por favor, seleccione opción</p>
    <ul class="actions">
        <li><a href="{{ URL::asset('main/ausentismo/nuevo')}}" class="button submit">Nuevo Ausentismo</a></li>
    </ul>
    <ul class="actions">
        <li><a href="{{ URL::asset('main/ausentismo/administrar')}}" class="button submit">Administrar Ausentismo</a></li>
    </ul>
    <ul class="actions">
        <li><a href="{{ URL::asset('main/ausentismo/asignuevo')}}" class="button submit">Asignar Ausentismo</a></li>
    </ul>
    <ul class="actions">
        <li><a href="{{ URL::asset('main/ausentismo/asigadministrar')}}" class="button submit">Administrar Asignaciones</a></li>
</ul>
@stop         

@section('submenu')
@include('menus.main')
@stop              

@section('subseccion11')
@include('menus.menus_ayuda')
@stop        