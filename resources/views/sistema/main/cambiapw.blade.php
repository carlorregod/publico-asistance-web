@extends('layouts.principal')

@section('bienvenida')
	<h1>AsisTance</h1>
	<p>Bienvenidos a Asistance.</p>
	@include('alertas.alerta')
	<p>Ingrese sus datos para cambiar su actual contraseña</p>
		<section>
			{!!Form::open(['url'=>'main/pwreset' ,'method'=>'POST'])!!}
				<div class="fields">
					<div class="field half">
						{!! Form::label('RUT', 'Escriba su RUT sin puntos ni guiones. Si su RUT termina en K reemplazar por 0 (cero): ') !!}
						{!! Form::text('RUT', null, ['class' => 'form-control', 'onkeypress' => 'return soloNumeroNatural(event)', 'maxlength' => '10','placeholder' =>'Ejemplo: 111111111']) !!}
						</div>
					<div class="field half">
						{!! Form::label('email', 'Escriba su correo electrónico: ') !!}<br/>
						{!! Form::email('email', null, ['class' => 'form-control', 'maxlength' => '40','placeholder' =>'Ejemplo: hola@hola.cl']) !!}
					</div>
					<div class="field half">
							{!! Form::label('pw1', 'Escriba su nueva contraseña: ') !!}
							{!! Form::text('pw1', null, ['class' => 'form-control', 'maxlength' => '30', 'minlength' =>'4']) !!}
					</div>
					<div class="field half">
							{!! Form::label('pw2', 'Escriba nuevamente su nueva contraseña: ') !!}
							{!! Form::text('pw2', null, ['class' => 'form-control', 'maxlength' => '30', 'minlength' =>'4']) !!}
					</div>
				</div>
					<br />
				<ul class="actions">
					{!! Form::submit('Restablecer contraseña',['class' => 'btn btn-primary']) !!}
				</ul>
			{!!Form::close()!!}
		</section>
@stop    

@section('submenu')
@include('menus.main')
@stop  

@section('subseccion11')
@include('menus.menus_ayuda')
@stop  
