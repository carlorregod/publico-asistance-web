@extends('layouts.principal')

@section('bienvenida')
    <h1>Usuarios</h1>
    <p>Bienvenidos a Asistance</p>
    <p>Por favor, seleccione opción</p>
    <ul class="actions">
            <li><a href="{{ URL::asset('main/usuarios/nuevo')}}" class="button submit">Nuevo Usuario</a></li>
    </ul>
    <ul class="actions">
            <li><a href="{{ URL::asset('main/usuarios/administrar')}}" class="button submit">Administrar Usuarios</a></li>
    </ul>
    <ul class="actions">
            <li><a href="{{ URL::asset('main/usuarios/celular')}}" class="button submit">Nuevo Celular</a></li>
    </ul>
    <ul class="actions">
            <li><a href="{{ URL::asset('main/usuarios/administrarcel')}}" class="button submit">Administrar Celulares</a></li>
    </ul>
@stop           

@section('submenu')
@include('menus.main')
@stop            

@section('subseccion11')
@include('menus.menus_ayuda')
@stop     