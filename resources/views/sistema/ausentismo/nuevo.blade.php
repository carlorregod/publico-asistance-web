@extends('layouts.principal')

@section('bienvenida')
    <h1>Ausentismo</h1>
    <p>Bienvenidos a Asistance</p>
	<p>Nuevo ausentismo. Favor complete formulario</p>
	@include('alertas.alerta')
	@include('alertas.validaformulario')
	<section>
		<!--Formulario para crear un nuevo ausentismo y ser almacenado a la BBDD -->
		{!! Form::open(['route' => 'main.ausentismo.administrar.store', 'method' => 'POST']) !!}
			<div class="fields">
				<div class="field half">
					{!! Form::label('Nombre_AusentismoFestivo', 'Alias o pseudónimo: ') !!}
					{!! Form::text('Nombre_AusentismoFestivo', null, ['class' => 'form-control', 'maxlength' => '24', 'placeholder' =>'Ejemplo: Vacación']) !!}
				</div>
				<div class="field half">
					{!! Form::label('pago', '¿Ausentismo remunerado?: ') !!}
					{!!	Form::select('pago', ['1' => 'Sí', '0' => 'No'], '1') !!}
				</div>
			</div>
			<ul class="actions">
				<li>
				{!! Form::submit('Generar nuevo ausentismo',['class' => 'btn btn-primary']) !!}
				</li>
			</ul>
		{!! Form::close() !!}
	</section>	
@stop            

@section('submenu')
@include('menus.ausentismo')
@stop           

@section('subseccion11')
@include('menus.menus_ayuda')
@stop       