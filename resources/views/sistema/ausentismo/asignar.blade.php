@extends('layouts.principal')

@section('bienvenida')
    <h1>Ausentismo</h1>
    <p>Bienvenidos a Asistance</p>
    <p>Administración de ausentismos. Asignación de ausentismos a trabajadores</p>
    
	@include('alertas.alerta')
	@include('alertas.validaformulario')
        <!-- Buscar -->
        <h4>Filtros de exportación tabla (rangos de fecha respecto a fecha asignación inicial)</h4>
        {!! Form::open(['route' => 'main.ausentismo.asigadministrar.show', 'method' => 'GET'])!!}
        <div class="fields">
            <div class="field half">
                {!! Form::label('RUT', 'Buscar por RUT: ') !!}
                <select name="RUT" id="RUT" class="form-control" required="required">
                    @foreach($usuarios as $usuario)
                        <option value="{{ $usuario->RUT}}" id="{{ $usuario->RUT}}" name="{{ $usuario->RUT}}">{{ $usuario->RUT}}: {{ $usuario->Apellido_P}} {{ $usuario->Apellido_M}} {{ $usuario->Nombres }}</option>
                    @endforeach
                </select>
                </div>
            <div class="field quarter">    
                {!! Form::label('fechaInicio', 'Buscar por fecha:  I') !!}         	
                {!! Form::date('fechaInicio', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
            </div>
            <div class="field quarter">    
                {!! Form::label('fechaFinal', 'Buscar por fecha: F ') !!}         	
                {!! Form::date('fechaFinal', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
            </div>
        </div>
        <p></p>
        {!! Form::submit('Mostrar resultados',['class' => 'button small']) !!}
        {!! Form::close()!!}
        <h4></h4>
        <!-- Exportar Excel -->
        <h4>Filtros de exportación tabla (rangos de fecha respecto a fecha asignación inicial)</h4>
        {!! Form::open(['url' => 'main/ausentismo/asigexport', 'method' => 'GET']) !!}
        <div class="fields">
            <div class="field quarter">    
                {!! Form::label('fechaInicio', 'Fecha inicio: ') !!}         	
                {!! Form::date('fechaInicio', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
            </div>
            <div class="field quarter">    
                {!! Form::label('fechaFinal', 'Fecha fin: ') !!}         	
                {!! Form::date('fechaFinal', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
            </div>
        </div>
        <p></p>
        {!! Form::submit('Exporta todas las asignaciones de ausentismos *xls',['class' => 'button']) !!}
        {!! Form::close()!!}
        <p></p>
        <!-- Tabla -->
        <h2>Tabla de Asignación Ausentismo</h2>
        {!! Form::open(['url' => 'main/ausentismo/asigadministrar/borra', 'method' => 'post']) !!}   
        <p></p>
        <!--Tabla de ocurrencias de ausentismos -->
        <table class="table" align="center" id="tblData">
                <thead>
                    <th>Operación</th>
                    <th align="center">Check</th>
                    <th>RUT</th>
                    <th>Tipo  <br/>ausentismo</th>
                    <th>Fecha Inicial</th>
                    <th>Fecha Final</th>
                </thead>
            @foreach($asigausentismos as $asigausentismo) 
                <tbody>
                    <td>{!! link_to_route('main.ausentismo.asigadministrar.edit', $title = 'Modificar', $parameters = $asigausentismo->contador, $attributes = ['class'=>'btn btn-primary']) !!}</td>
                    <td align="center"><input id="{{$asigausentismo->contador}}" value="{{$asigausentismo->contador}}" name="delete[]" type="checkbox" class="checkthis"/><label for="{{$asigausentismo->contador}}"></label></td>
                    <td>{{$asigausentismo->RUT}}</td>
                    <td>{{$asigausentismo->Nombre_AusentismoFestivo}}</td>
                    <td>{{ date('d-m-Y',strtotime($asigausentismo->Fecha_inicio))  }}</td>
                    <td>{{ date('d-m-Y',strtotime($asigausentismo->Fecha_fin))  }}</td>
                </tbody>
            @endforeach
        </table>
    {!! $asigausentismos->render()!!}
    <p></p>
    <!-- Eliminación masiva -->
    {!! Form::submit('Eliminar check de asociaciones',['class' => 'button small']) !!}
    {!! Form::close() !!}		
    
@stop            

@section('submenu')
@include('menus.ausentismo')
@stop           

@section('subseccion11')
@include('menus.menus_ayuda')
@stop    