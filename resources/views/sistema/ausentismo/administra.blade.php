@extends('layouts.principal')

@section('bienvenida')
    <h1>Ausentismo</h1>
    <p>Bienvenidos a Asistance</p>
    <p>Administración de ausentismos. Especificar operación</p>
   
	@include('alertas.alerta')
	@include('alertas.validaformulario')

    <!-- Buscar -->
    {!! Form::open(['route' => 'main.ausentismo.administrar.show', 'method' => 'GET'])!!}
    <div class="fields">
		<div class="field half">
            {!! Form::label('Nombre_AusentismoFestivo', 'Buscas por alias de ausentismo: ') !!}
            {!! Form::text('Nombre_AusentismoFestivo', null, ['class' => 'form-control', 'maxlength' => '24', 'placeholder' =>'Ejemplo: Vacación']) !!}   
        </div>
	</div>    
    <p></p>
    {!! Form::submit('Mostrar resultados',['class' => 'button small']) !!}
    {!! Form::close()!!}
    <!-- Exportar Excel -->
    <a href="{{ url('main/ausentismo/export')}}" class="button submit">Exporta todos los Ausentismos *xls</a>
    <p></p>
    <!-- Tabla -->
    <h2>Tabla de Ausentismos</h2>
        {!! Form::open(['url' => 'main/ausentismo/administrar/borra', 'method' => 'post']) !!}
        <p></p>
        <table class="table" align="center" size="small">
                <thead>
                    <th>Operación</th>
                    <th align="center">Check</th>
                    <th>Ausentismo</th>
                    <th>Validador de <br/>remuneración</th>
                </thead>
            @foreach($ausentismos as $ausentismo) 
                <tbody>
                    <td>{!! link_to_route('main.ausentismo.administrar.edit', $title = 'Modificar', $parameters = $ausentismo->id_ausen_fest , $attributes = ['class'=>'btn btn-primary']) !!}</td>
                    <td align="center"><input id="{{$ausentismo->id_ausen_fest}}" value="{{$ausentismo->id_ausen_fest}}" name="delete[]" type="checkbox" class="checkthis"/><label for="{{$ausentismo->id_ausen_fest}}"></label></td>
                    <td>{{$ausentismo->Nombre_AusentismoFestivo}}</td>
                    <td>                        
                        @if($ausentismo->pago === 0)
                        No
                        @else
                        Sí     
                        @endif
                    </td>
                </tbody>
            @endforeach
        </table>
    {!! $ausentismos->render()!!}
    <p></p>
    <!-- Eliminación masiva -->
    {!! Form::submit('Eliminar check',['class' => 'button small']) !!}
    {!! Form::close() !!}
@stop            

@section('submenu')
@include('menus.ausentismo')
@stop           

@section('subseccion11')
@include('menus.menus_ayuda')
@stop    
