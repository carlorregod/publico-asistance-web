@extends('layouts.principal')

@section('bienvenida')
    <h1>Ausentismo</h1>
    <p>Bienvenidos a Asistance</p>
    <p>Administración de asignación de ausentismos. Efectúe su edición</p>
    @include('alertas.alerta')
	@include('alertas.validaformulario')
    {!! Form::model($asigausentismo, ['route' => ['main.ausentismo.asigadministrar.update', $asigausentismo->contador], 'method' => 'PUT']) !!}
    <div class="fields">
        <div class="field half">
            {!! Form::label('RUT', 'RUT del trabajador: ') !!}
            {!! Form::text('RUT', null, ['class' => 'form-control', 'readonly'=>'readonly']) !!}
        </div>
        <div class="field half">
            {!! Form::label('Ausentismo', 'Seleccione ausentismo: ') !!}
            <select name="id_ausen_fest" id="id_ausen_fest" class="form-control" required="required">
                @foreach($ausentismos as $ausentismo)
                    <option value="{{ $ausentismo->id_ausen_fest}}" id="{{ $ausentismo->id_ausen_fest}}" name="{{ $ausentismo->id_ausen_fest}}">{{ $ausentismo->Nombre_AusentismoFestivo }}</option>
                @endforeach
            </select>
        </div>
        <div class="field half">
            {!! Form::label('Fecha_inicio', 'Especifique fecha inicio del ausentismo: ') !!}
            {!! Form::date('Fecha_inicio', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
            </div>
        <div class="field half">
            {!! Form::label('Fecha_fin', 'Seleccionar fecha final del ausentismo: ') !!}
            {!! Form::date('Fecha_fin', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
        </div>
    </div>
    <ul class="actions">
    <li>
    {!! Form::submit('Actualizar',['class' => 'btn btn-primary']) !!}
    </li>
    </ul>
    {!! Form::close() !!}

    {!! Form::open(['route' => ['main.ausentismo.asigadministrar.destroy', $asigausentismo->contador], 'method' => 'DELETE']) !!}
        <ul class="actions">
    <li>{!! Form::submit('Eliminar',['class' => 'btn btn-danger']) !!}</li>
        </ul>
    {!! Form::close() !!}
@stop            

@section('submenu')
@include('menus.ausentismo')
@stop           

@section('subseccion11')
@include('menus.menus_ayuda')
@stop    