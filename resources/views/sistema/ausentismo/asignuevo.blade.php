@extends('layouts.principal')

@section('bienvenida')
    <h1>Ausentismo</h1>
    <p>Bienvenidos a Asistance</p>
	<p>Nueva asignación de ausentismo. Favor complete formulario</p>
	@include('alertas.alerta')
	@include('alertas.validaformulario')
	<section>
		<!--Formulario para crear una nueva asociación de ausentismo y ser almacenado a la BBDD -->
		{!! Form::open(['route' => 'main.ausentismo.asigadministrar.store', 'method' => 'POST']) !!}
			<div class="fields">
				<div class="field half">
					{!! Form::label('RUT', 'Seleccionar RUT del trabajador: ') !!}
                    <select name="RUT" id="RUT" class="form-control" required="required">
						@foreach($usuarios as $usuario)
							<option value="{{ $usuario->RUT}}" id="{{ $usuario->RUT}}" name="{{ $usuario->RUT}}">{{ $usuario->RUT}}: {{ $usuario->Apellido_P}} {{ $usuario->Apellido_M}} {{ $usuario->Nombres }}</option>
						@endforeach
					</select>
				</div>
				<div class="field half">
					{!! Form::label('Ausentismo', 'Seleccione ausentismo: ') !!}
					<select name="id_ausen_fest" id="id_ausen_fest" class="form-control" required="required">
						@foreach($ausentismos as $ausentismo)
							<option value="{{ $ausentismo->id_ausen_fest}}" id="{{ $ausentismo->id_ausen_fest}}" name="{{ $ausentismo->id_ausen_fest}}">{{ $ausentismo->Nombre_AusentismoFestivo }}</option>
						@endforeach
					</select>
                </div>
                <div class="field half">
                    {!! Form::label('Fecha_inicio', 'Especifique fecha inicio del ausentismo: ') !!}
                    {!! Form::date('Fecha_inicio', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
                    </div>
                <div class="field half">
                    {!! Form::label('Fecha_fin', 'Seleccionar fecha final del ausentismo: ') !!}
                    {!! Form::date('Fecha_fin', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
                </div>
			</div>
			<ul class="actions">
				<li>
				{!! Form::submit('Generar nueva asociación',['class' => 'btn btn-primary']) !!}
				</li>
			</ul>
		{!! Form::close() !!}
	</section>	
@stop            

@section('submenu')
@include('menus.ausentismo')
@stop           

@section('subseccion11')
@include('menus.menus_ayuda')
@stop       