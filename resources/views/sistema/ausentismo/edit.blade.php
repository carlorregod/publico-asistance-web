@extends('layouts.principal')

@section('bienvenida')
    <h1>Ausentismo</h1>
    <p>Bienvenidos a Asistance</p>
    <p>Administración de ausentismos. Efectúe su edición</p>
    @include('alertas.alerta')
	@include('alertas.validaformulario')
    {!! Form::model($ausentismo, ['route' => ['main.ausentismo.administrar.update', $ausentismo->id_ausen_fest], 'method' => 'PUT']) !!}
        <div class="fields">
            <div class="field half">            
                {!! Form::label('Nombre_AusentismoFestivo', 'Alias del ausentismo a actualizar: ') !!}
                {!! Form::text('Nombre_AusentismoFestivo', null, ['class' => 'form-control', 'readonly'=>'readonly']) !!}
            </div>
            <div class="field half">
                {!! Form::label('pago', '¿Ausentismo remunerado?: ') !!}
                {!!	Form::select('pago', ['1' => 'Sí', '0' => 'No'], '1') !!}
            </div>
        </div>
    <ul class="actions">
    <li>
    {!! Form::submit('Actualizar',['class' => 'btn btn-primary']) !!}
    </li>
    </ul>
    {!! Form::close() !!}

    {!! Form::open(['route' => ['main.ausentismo.administrar.destroy', $ausentismo->id_ausen_fest], 'method' => 'DELETE']) !!}
        <ul class="actions">
    <li>{!! Form::submit('Eliminar',['class' => 'btn btn-danger']) !!}</li>
        </ul>
    {!! Form::close() !!}
@stop            

@section('submenu')
@include('menus.ausentismo')
@stop           

@section('subseccion11')
@include('menus.menus_ayuda')
@stop    