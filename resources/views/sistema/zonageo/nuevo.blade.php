@extends('layouts.principal')

@section('bienvenida')
    <h1>Zonas Geográficas</h1>
    <p>Bienvenidos a Asistance</p>
	<p>Por favor, complete el formulario.</p>
	
	@include('alertas.alerta')
	@include('alertas.validaformulario')
	<section>
			{!! Form::open(['route' => 'main.zonageo.administrar.store', 'method' => 'POST']) !!}
				<div class="fields">
					<div class="field">
					{!! Form::label('Alias_loc_geo', 'Alias o pseudónimo: ') !!}
					{!! Form::text('Alias_loc_geo', null, ['class' => 'form-control', 'maxlength' => '24', 'placeholder' =>'Ejemplo: ZONAGEO-0001']) !!}
					</div>
					<h4>Arrastre los marcadores del mapa para captuar ambos puntos del área geográfica a establecer</h4>
					<h5>La fecha azul debe ubicarse en la zona inferior izquierda del mapa. </h5><br>
					<h5> La fecha roja debe ubicarse en la zona superior derecha del mapa. </h5><br>
					<h4>No olvide aceptar que Google permita detectar su posición actual. </h4><br />
					<div class="field" id="map"style="width: 800px; height:600px;"><br /></div>
					<div class="field half">
						{!! Form::label('coorX1', 'Componente X primer vértice superior izquierdo: ') !!}
						<input type="text" name="coorX1" id="long1" readonly="readonly" />
						{!! Form::label('coorY1', 'Componente Y primer vértice superior izquierdo: ') !!}
						<input type="text" name="coorY1" id="lat1" readonly="readonly" />
					</div>
					<div class="field half">
						{!! Form::label('coorX2', 'Componente X segundo vértice superior izquierdo: ') !!}
						<input type="text" name="coorX2" id="long2" readonly="readonly" />
						{!! Form::label('coorY2', 'Componente Y segundo vértice superior izquierdo: ') !!}
						<input type="text" name="coorY2" id="lat2" readonly="readonly" />
					</div>
				</div>
                <ul class="actions">
					<li>
						{!! Form::submit('Generar nueva zona',['class' => 'btn btn-primary']) !!}
					</li>
                </ul>
			{!! Form::close() !!}
		</section>
		<script src="{{ URL::asset('assets/js/locgeo.js')}}"></script>
@stop         

@section('submenu')
@include('menus.zonageo')
@stop         

@section('subseccion11')
@include('menus.menus_ayuda') 
<!-- La clave de API acá la obtuve de cuenta Google Cloud: AIzaSyBz2P8cnKkLgEjK2h2C1dtfD8B8gRmEARI --> 
<script async defer src="https://maps.googleapis.com/maps/api/js?key=
AIzaSyBz2P8cnKkLgEjK2h2C1dtfD8B8gRmEARI&callback=initMap"
type="text/javascript"></script>
@stop 