@extends('layouts.principal')

@section('bienvenida')
    <h1>Zonas Geográficas</h1>
    <p>Bienvenidos a Asistance</p>
	<p>Nueva asignación de zona geográfica. Favor complete formulario</p>
	@include('alertas.alerta')
	@include('alertas.validaformulario')
	<section>
		<!--Formulario para crear una nueva asociación de zona geográfica y ser almacenado a la BBDD -->
		{!! Form::open(['route' => 'main.zonageo.asigadministrar.store', 'method' => 'POST']) !!}
			<div class="fields">
				<div class="field">
					{!! Form::label('RUT', 'Seleccionar RUT del trabajador: ') !!}
                    <select name="RUT" id="RUT" class="form-control" required="required">
						@foreach($usuarios as $usuario)
							<option value="{{ $usuario->RUT}}" id="{{ $usuario->RUT}}" name="{{ $usuario->RUT}}">{{ $usuario->RUT}}: {{ $usuario->Apellido_P}} {{ $usuario->Apellido_M}} {{ $usuario->Nombres }}</option>
						@endforeach
					</select>
				</div>
				<div class="field half">
					{!! Form::label('ZonaGeo', 'Seleccione alias zona geográfica: ') !!}
					<select name="id_loc_geo" id="id_loc_geo" class="form-control" required="required">
						@foreach($zonageos as $zonageo)
							<option value="{{ $zonageo->id_loc_geo}}" id="{{ $zonageo->id_loc_geo}}" name="{{ $zonageo->id_loc_geo}}">{{ $zonageo->Alias_loc_geo }}</option>
						@endforeach
					</select>
                </div>
                <div class="field half">
					{!! Form::label('TipoMarca', 'Seleccione el tipo de marca: ') !!}
					<select name="id_marcado" id="id_marcado" class="form-control" required="required">
						@foreach($tipomarcas as $tipomarca)
							<option value="{{ $tipomarca->id_marcado}}" id="{{ $tipomarca->id_marcado}}" name="{{ $tipomarca->id_marcado}}">{{ $tipomarca->Tipo_marca }}</option>
						@endforeach
					</select>
                </div>
                <div class="field half">
                    {!! Form::label('Fecha_inicio', 'Especifique fecha inicio de la asignación de zona: ') !!}
                    {!! Form::date('Fecha_inicio', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
                    </div>
                <div class="field half">
                    {!! Form::label('Fecha_fin', 'Especifique fecha final de la asignación de zona: ') !!}
                    {!! Form::date('Fecha_fin', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
                </div>
			</div>
			<ul class="actions">
				<li>
				{!! Form::submit('Generar nueva asociación',['class' => 'btn btn-primary']) !!}
				</li>
			</ul>
		{!! Form::close() !!}
	</section>	
@stop            

@section('submenu')
@include('menus.zonageo')
@stop           

@section('subseccion11')
@include('menus.menus_ayuda')
@stop       