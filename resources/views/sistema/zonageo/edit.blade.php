@extends('layouts.principal')

@section('bienvenida')
    <h1>Zonas Geográficas</h1>
    <p>Bienvenidos a Asistance</p>
    <p>Administración de zonas geográficas. Efectúe su edición</p>
    <!-- Formulario de edición -->
    @include('alertas.alerta')
	@include('alertas.validaformulario')
    <section>
    {!! Form::model($zonageo, ['route' => ['main.zonageo.administrar.update', $zonageo->id_loc_geo], 'method' => 'PUT']) !!}
    <div class="fields">
            <div class="field">
                {!! Form::label('Alias_loc_geo', 'Alias de zona geográfica a editar: ') !!}
                {!! Form::text('Alias_loc_geo', null, ['class' => 'form-control', 'readonly'=>'readonly']) !!}
            </div>
            <h4>Arrastre los marcadores del mapa para captuar ambos puntos del área geográfica a establecer</h4>
            <h5>La fecha azul debe ubicarse en la zona inferior izquierda del mapa. </h5><br>
            <h5> La fecha roja debe ubicarse en la zona superior derecha del mapa. </h5><br>
            <h4>No olvie aceptar que Google permita detectar su posición actual. </h4>
            <div class="field" id="map"style="width: 800px; height:600px;"><br /></div>
            <div class="field half">
                {!! Form::label('coorX1', 'Componente X primer vértice superior izquierdo: ') !!}
                <input type="text" name="coorX1" id="long1" readonly="readonly" />
                {!! Form::label('coorY1', 'Componente Y primer vértice superior izquierdo: ') !!}
                <input type="text" name="coorY1" id="lat1" readonly="readonly" />
            </div>
            <div class="field half">
                {!! Form::label('coorX2', 'Componente X segundo vértice superior izquierdo: ') !!}
                <input type="text" name="coorX2" id="long2" readonly="readonly" />
                {!! Form::label('coorY2', 'Componente Y segundo vértice superior izquierdo: ') !!}
                <input type="text" name="coorY2" id="lat2" readonly="readonly" />
            </div>
        </div>
    <ul class="actions">
    <li>
    {!! Form::submit('Actualizar',['class' => 'btn btn-primary']) !!}
    </li>
    </ul>
    {!! Form::close() !!}
    <!-- Botón de elimnación de registros -->
    {!! Form::open(['route' => ['main.zonageo.administrar.destroy', $zonageo->id_loc_geo], 'method' => 'DELETE']) !!}
        <ul class="actions">
    <li>{!! Form::submit('Eliminar',['class' => 'btn btn-danger']) !!}</li>
        </ul>
    {!! Form::close() !!}
    </section>
    <script src="{{ URL::asset('assets/js/locgeo.js')}}"></script>
@stop            

@section('submenu')
@include('menus.zonageo')
@stop         

@section('subseccion11')
@include('menus.menus_ayuda')
<!-- La clave de API acá la obtuve de cuenta Google Cloud: AIzaSyBz2P8cnKkLgEjK2h2C1dtfD8B8gRmEARI --> 
<script async defer src="https://maps.googleapis.com/maps/api/js?key=
AIzaSyBz2P8cnKkLgEjK2h2C1dtfD8B8gRmEARI&callback=initMap"
type="text/javascript"></script>
@stop 