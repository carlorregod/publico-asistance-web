@extends('layouts.principal')

@section('bienvenida')
    <h1>Zonas Geográficas</h1>
    <p>Bienvenidos a Asistance</p>
    <p>Administración de zonas geográficas. Especificar operación</p>
    
	@include('alertas.alerta')
    @include('alertas.validaformulario')

    <!-- Buscar -->
    {!! Form::open(['route' => 'main.zonageo.administrar.show', 'method' => 'GET'])!!}
    <div class="fields">
        <div class="field half">
        {!! Form::label('Alias_loc_geo', 'Buscar por alias de Zona Geográfica: ') !!}
        {!! Form::text('Alias_loc_geo', null, ['class' => 'form-control', 'maxlength' => '16', 'value' => old('Alias_loc_geo'), 'placeholder' =>'Ejemplo: ZONAGEO-0001']) !!}
        </div>
    </div>      
    <p></p>
    {!! Form::submit('Mostrar resultados',['class' => 'button small']) !!}
    {!! Form::close()!!}
    <h4></h4>
    <!-- Exportar Excel -->
    <a href="{{ url('main/zonageo/export')}}" class="button submit">Exporta todas las zonas geográficas *xls</a>
    <p></p>
    <!-- Tabla -->
    <h2>Tabla de Zonas Geográficas</h2>
        {!! Form::open(['url' => 'main/zonageo/administrar/borra', 'method' => 'post']) !!}
        <table class="table" align="center" size="small">
                <thead>
                    <th>Operación</th>
                    <th>Check</th>
                    <th>Alias Zona geográfica</th>
                    <th>X1</th>
                    <th>Y1</th>
                    <th>X2</th>
                    <th>Y2</th>
                </thead>
            @foreach($zonageos as $zonageo) 
                <tbody>
                    <td>{!! link_to_route('main.zonageo.administrar.edit', $title = 'Modificar', $parameters = $zonageo->id_loc_geo , $attributes = ['class'=>'btn btn-primary']) !!}</td>
                    <td align="center"><input id="{{$zonageo->id_loc_geo}}" value="{{$zonageo->id_loc_geo}}" name="delete[]" type="checkbox" class="checkthis"/><label for="{{$zonageo->id_loc_geo}}"></label></td>
                    <td>{{$zonageo->Alias_loc_geo}}</td>
                    <td>{{$zonageo->coorX1}}</td>
                    <td>{{$zonageo->coorY1}}</td>
                    <td>{{$zonageo->coorX2}}</td>
                    <td>{{$zonageo->coorY2}}</td>
                </tbody>
            @endforeach
        </table>	
        {!! $zonageos->render()!!}
        <p></p>
        <!-- Eliminación masiva -->
        {!! Form::submit('Eliminar check',['class' => 'button small']) !!}
        {!! Form::close() !!}  
@stop            

@section('submenu')
@include('menus.zonageo')
@stop         

@section('subseccion11')
@include('menus.menus_ayuda')
@stop 