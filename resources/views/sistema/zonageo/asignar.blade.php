@extends('layouts.principal')

@section('bienvenida')
    <h1>Zonas Geográficas</h1>
    <p>Bienvenidos a Asistance</p>
    <p>Administración de asignación de zonas geográficas a trabajadores</p>
    
	@include('alertas.alerta')
	@include('alertas.validaformulario')
        <!-- Buscar -->
        <h4>Filtros de visualización (rangos asociados a la fecha inicial de la asociación)</h4>
        {!! Form::open(['route' => 'main.zonageo.asigadministrar.show', 'method' => 'GET'])!!}
        <div class="fields">
            <div class="field half">
                {!! Form::label('RUT', 'Buscar por RUT del usuario: ') !!}<br/>
                <select name="RUT" id="RUT" class="form-control" required="required">
                @foreach($usuarios as $usuario)
                    <option value="{{ $usuario->RUT}}" id="{{ $usuario->RUT}}" name="{{ $usuario->RUT}}">{{ $usuario->RUT}}: {{ $usuario->Apellido_P}} {{ $usuario->Apellido_M}} {{ $usuario->Nombres }}</option>
                @endforeach
                </select>
            </div>
            <div class="field quarter">    
                {!! Form::label('fechaInicio', 'Buscar por fecha:  Inical') !!}         	
                {!! Form::date('fechaInicio', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
            </div>
            <div class="field quarter">    
                {!! Form::label('fechaFinal', 'Buscar por fecha: Final ') !!}         	
                {!! Form::date('fechaFinal', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
            </div>
        </div>
        <p></p>
        {!! Form::submit('Mostrar resultados',['class' => 'button small']) !!}
        {!! Form::close()!!}
        <h4></h4>
        <!-- Exportar Excel -->
        <h4>Filtros de exportación tabla (rangos asociados a la fecha inicial de la asociación)</h4>
        {!! Form::open(['url' => 'main/zonageo/asigexport', 'method' => 'GET']) !!}
        <div class="fields">
            <div class="field quarter">    
                {!! Form::label('fechaInicio', 'Fecha inicio: ') !!}         	
                {!! Form::date('fechaInicio', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
            </div>
            <div class="field quarter">    
                {!! Form::label('fechaFinal', 'Fecha fin: ') !!}         	
                {!! Form::date('fechaFinal', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
            </div>
        </div>
        <p></p>
        {!! Form::submit('Exporta todas las asignaciones de zonas geográficas *xls',['class' => 'button']) !!}
        {!! Form::close()!!}
        <p></p>
        <!-- Tabla -->
        <h2>Tabla de Asignación Zonas Geográficas</h2>
        {!! Form::open(['url' => 'main/zonageo/asigadministrar/borra', 'method' => 'post']) !!}
        <p></p>
		<!--Tabla de ocurrencias de zonas geográficas -->
        <table class="table" align="center" size="small">
                <thead>
                    <th>Operación</th>
                    <th align="center">Check</th>
                    <th>RUT</th>
                    <th>Alias  zona<br/>geográfica</th>
                    <th>Tipo marca</th>
                    <th>Fecha Inicial</th>
                    <th>Fecha Final</th>
                </thead>
            @foreach($asigzonageos as $asigzonageo) 
                <tbody>
                    <td>{!! link_to_route('main.zonageo.asigadministrar.edit', $title = 'Modificar', $parameters = $asigzonageo->contador, $attributes = ['class'=>'btn btn-primary']) !!}</td>
                    <td align="center"><input id="{{$asigzonageo->contador}}" value="{{$asigzonageo->contador}}" name="delete[]" type="checkbox" class="checkthis"/><label for="{{$asigzonageo->contador}}"></label></td>
                    <td>{{$asigzonageo->RUT}}</td>
                    <td>{{$asigzonageo->Alias_loc_geo}}</td>
                    <td>{{$asigzonageo->Tipo_marca}}</td>
                    <td>{{ date('d-m-Y',strtotime($asigzonageo->Fecha_inicio)) }}</td>
                    <td>{{ date('d-m-Y',strtotime($asigzonageo->Fecha_fin)) }}</td>
                </tbody>
            @endforeach
        </table>
    {!! $asigzonageos->render()!!}
    <p></p>
    <!-- Eliminación masiva -->
    {!! Form::submit('Eliminar check de asociaciones',['class' => 'button small']) !!}
    {!! Form::close() !!}	    	
@stop            

@section('submenu')
@include('menus.zonageo')
@stop             

@section('subseccion11')
@include('menus.menus_ayuda')
@stop    