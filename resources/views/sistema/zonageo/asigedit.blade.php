@extends('layouts.principal')

@section('bienvenida')
    <h1>Zonas Geográficas</h1>
    <p>Bienvenidos a Asistance</p>
    <p>Administración de asignación de zonas geográficas. Efectúe su edición</p>
    @include('alertas.alerta')
	@include('alertas.validaformulario')
    {!! Form::model($asigzonageo, ['route' => ['main.zonageo.asigadministrar.update', $asigzonageo->contador], 'method' => 'PUT']) !!}
    <div class="fields">
        <div class="field">
            {!! Form::label('RUT', 'RUT del trabajador: ') !!}
            {!! Form::text('RUT', null, ['class' => 'form-control', 'readonly'=>'readonly']) !!}
        </div>
        <div class="field half">
            {!! Form::label('ZonaGeo', 'Seleccione alias zona geográfica: ') !!}
            <select name="id_loc_geo" id="id_loc_geo" class="form-control" required="required">
                @foreach($zonageos as $zonageo)
                    <option value="{{ $zonageo->id_loc_geo}}" id="{{ $zonageo->id_loc_geo}}" name="{{ $zonageo->id_loc_geo}}">{{ $zonageo->Alias_loc_geo }}</option>
                @endforeach
            </select>
        </div>
        <div class="field half">
            {!! Form::label('TipoMarca', 'Seleccione el tipo de marca: ') !!}
            <select name="id_marcado" id="id_marcado" class="form-control" required="required">
                @foreach($tipomarcas as $tipomarca)
                    <option value="{{ $tipomarca->id_marcado}}" id="{{ $tipomarca->id_marcado}}" name="{{ $tipomarca->id_marcado}}">{{ $tipomarca->Tipo_marca }}</option>
                @endforeach
            </select>
        </div>
        <div class="field half">
            {!! Form::label('Fecha_inicio', 'Especifique fecha inicio de la asignación de zona: ') !!}
            {!! Form::date('Fecha_inicio', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
            </div>
        <div class="field half">
            {!! Form::label('Fecha_fin', 'Especifique fecha final de la asignación de zona: ') !!}
            {!! Form::date('Fecha_fin', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
        </div>
    </div>
    <ul class="actions">
    <li>
    {!! Form::submit('Actualizar',['class' => 'btn btn-primary']) !!}
    </li>
    </ul>
    {!! Form::close() !!}

    {!! Form::open(['route' => ['main.zonageo.asigadministrar.destroy', $asigzonageo->contador], 'method' => 'DELETE']) !!}
        <ul class="actions">
    <li>{!! Form::submit('Eliminar',['class' => 'btn btn-danger']) !!}</li>
        </ul>
    {!! Form::close() !!}
@stop            

@section('submenu')
@include('menus.zonageo')
@stop           
         

@section('subseccion11')
@include('menus.menus_ayuda')
@stop    