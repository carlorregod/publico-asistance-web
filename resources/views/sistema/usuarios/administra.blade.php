@extends('layouts.principal')

@section('bienvenida')
    <h1>Usuarios</h1>
    <p>Bienvenidos a Asistance. Administración de usuarios</p>
    <p>Buscar</p>
    @include('alertas.alerta')
    @include('alertas.validaformulario')

        {!! Form::open(['route' => 'main.usuarios.administrar.show', 'method' => 'GET'])!!}
        <div class="fields">
            <div class="field quarter">
            {!! Form::label('RUT', 'Buscar por rut de usuario: ') !!}
            {!! Form::text('RUT', null, ['class' => 'form-control', 'onkeypress' => 'return soloNumeroNatural(event)', 'maxlength' => '10', 'value' => old('RUT'), 'placeholder' => 'Ejemplo: 123452157']) !!}
            </div>
            <div class="field quarter">
            {!! Form::label('email', 'Buscar por correo de usuario: ') !!}
            {!! Form::text('email', null, ['class' => 'form-control', 'maxlength' => '40', 'value' => old('email'), 'placeholder' => 'Ejemplo: hola@hola.cl']) !!}
            </div>
            <div class="field quarter">
            {!! Form::label('Apellido_P', 'Ingrese apellido paterno: ') !!}
            {!! Form::text('Apellido_P', null, ['class' => 'form-control', 'maxlength' => '15', 'value' => old('Apellido_P'), 'placeholder' => 'Ejemplo: González']) !!}
            </div>
            <div class="field quarter">
            {!! Form::label('Apellido_M', 'Ingrese apellido materno: ') !!}
            {!! Form::text('Apellido_M', null, ['class' => 'form-control', 'maxlength' => '15', 'value' => old('Apellido_M'), 'placeholder' =>'Ejemplo: Pérez']) !!}
            </div>
            <div class="field half">
            {!! Form::label('Nombres', 'Ingrese sus nombres: ') !!}
            {!! Form::text('Nombres', null, ['class' => 'form-control', 'maxlength' => '25', 'value' => old('Nombres'), 'placeholder' => 'Ejemplo: Juan Carlos']) !!}
            </div>
        </div>
        <p></p>    
        {!! Form::submit('Mostrar resultados',['class' => 'button small']) !!}
        {!! Form::close()!!}
        <h4></h4>
        <a href="{{ url('main/usuarios/export')}}" class="button submit">Exporta todos los usuarios *xls</a>
        <p></p>
        <h2>Tabla de usuarios</h2>
        {!! Form::open(['url' => 'main/usuarios/administrar/borra', 'method' => 'POST']) !!}
        <table class="table" align="center" size="small">
                <thead> 
                    <th>Operación</th>
                    <th align="center">Check</th>
                    <th>RUT</th>
                    <th>Nombre</th>
                    <th>Rol</th>
                    <th>Celular</th>
                </thead>
            @foreach($usuarios as $usuario) 
            <!-- Condición para que la tabla de ediciones no muestre al usuario activo de la sesión -->
            @if($usuario->RUT != Auth::user()->RUT )
                <tbody>
                    <td>{!! link_to_route('main.usuarios.administrar.edit', $title = 'Modificar', $parameters = $usuario->RUT , $attributes = ['class'=>'btn btn-primary']) !!}</td>
                    <td align="center"><input id="{{$usuario->RUT}}" value="{{$usuario->RUT}}" name="delete[]" type="checkbox" class="checkthis"/><label for="{{$usuario->RUT}}"></label></td>
                    <td>{{$usuario->RUT}}</td>
                    <td>{{$usuario->Apellido_P}} {{$usuario->Apellido_M}} {{$usuario->Nombres}}</td>
                    <td>{{$usuario->Rol}}</td>
                    <td>{{$usuario->Numero_celular}}</td>
                </tbody>
            @endif
            @endforeach
        </table>	
        {!! $usuarios->render()!!}
        <br/><br/>
        {!! Form::submit('Eliminar check',['class' => 'button small']) !!}
        {!! Form::close()!!}
@stop            

@section('submenu')
@include('menus.usuarios')
@stop           

@section('subseccion11')
@include('menus.menus_ayuda')
@stop  