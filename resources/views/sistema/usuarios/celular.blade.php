@extends('layouts.principal')

@section('bienvenida')
    <h1>Usuarios</h1>
    <p>Bienvenidos a Asistance</p>
	<p>Nuevo celular a agregar. Favor complete formulario</p>
	
	@include('alertas.alerta')
	@include('alertas.validaformulario')
	<section>
		<!--Formulario para crear un nuevo celular y ser almacenado a la BBDD -->
		{!! Form::open(['route' => 'main.usuarios.administrarcel.store', 'method' => 'POST']) !!}
			<div class="fields">
				<div class="field half">
					{!! Form::label('Numero_celular', 'Ingrese número celular: ') !!}
					{!! Form::text('Numero_celular', null, ['class' => 'form-control', 'onkeypress' => 'return soloNumeroNatural(event)', 'minlength' => '7', 'maxlength' => '11', 'placeholder' =>'Ejemplo: 99872345']) !!}
                </div>
                <div class="field half">
                    {!! Form::label('Compania', 'Ingrese compañía (opcional): ') !!}
                    {!! Form::text('Compania', null, ['class' => 'form-control', 'maxlength' => '18', 'placeholder' =>'Ejemplo: Entel']) !!}
                    </div>
				<div class="field half">
                    {!! Form::label('IMEI', 'Ingrese IMEI del equipo (opcional): ') !!}
                    {!! Form::text('IMEI', null, ['class' => 'form-control', 'maxlength' => '18','placeholder' =>'Ejemplo: 1111-1111-1111']) !!}
				</div>
			</div>
			<ul class="actions">
				<li>
				{!! Form::submit('Generar nuevo ceular',['class' => 'btn btn-primary']) !!}
				</li>
			</ul>
		{!! Form::close() !!}
	</section>	
@stop            

@section('submenu')
@include('menus.usuarios')
@stop           

@section('subseccion11')
@include('menus.menus_ayuda')
@stop  