@extends('layouts.principal')

@section('bienvenida')
    <h1>Usuarios</h1>
    <p>Bienvenidos a Asistance</p>
    <p>Administración de usuarios (celular). Especificar operación</p>
    
    @include('alertas.alerta')
    @include('alertas.validaformulario')
    <!-- Buscar -->
    {!! Form::open(['route' => 'main.usuarios.administrarcel.show', 'method' => 'GET'])!!}
        <div class="fields">
            <div class="field quarter">
            {!! Form::label('Numero_celular', 'Buscar por celular: ') !!}
            {!! Form::text('Numero_celular', null, ['class' => 'form-control', 'onkeypress' => 'return soloNumeroNatural(event)', 'maxlength' => '11', 'value' => old('Numero_celular'), 'placeholder' =>'Ejemplo: 99872345']) !!}
            </div>
        </div>
    <p></p>    
    {!! Form::submit('Mostrar resultados',['class' => 'button small']) !!}
    {!! Form::close()!!}
    <h4></h4>
    <!-- Exportar Excel -->
    <a href="{{ url('main/usuarios/exportcel')}}" class="button submit">Exporta todos los celulares *xls</a>
    <p></p>
    <!-- Tabla -->
    <h2>Tabla de celulares</h2>
        {!! Form::open(['url' => 'main/usuarios/administrarcel/borra', 'method' => 'post']) !!}
        <table class="table" align="center" size="small">
                <thead>
                    <th>Operación</th>
                    <th align="center">Check</th>
                    <th>Número celular</th>
                    <th>Compañía</th>
                    <th>IMEI</th>
                </thead>
            @foreach($celulars as $celular) 
                <tbody>
                    <td>{!! link_to_route('main.usuarios.administrarcel.edit', $title = 'Modificar', $parameters = $celular->id_celular , $attributes = ['class'=>'btn btn-primary']) !!}</td>
                    <td align="center"><input id="{{$celular->id_celular}}" value="{{$celular->id_celular}}" name="delete[]" type="checkbox" class="checkthis"/><label for="{{$celular->id_celular}}"></label></td>
                    <td>{{$celular->Numero_celular}}</td>
                    <td>{{$celular->Compania}}</td>
                    <td>{{$celular->IMEI}}</td>
                </tbody>
            @endforeach
        </table>
        {!! $celulars->render()!!}
        <p></p>
        <!-- Eliminación masiva -->
        {!! Form::submit('Eliminar check',['class' => 'button small']) !!}
        {!! Form::close()!!}
@stop            

@section('submenu')
@include('menus.usuarios')
@stop           

@section('subseccion11')
@include('menus.menus_ayuda')
@stop  