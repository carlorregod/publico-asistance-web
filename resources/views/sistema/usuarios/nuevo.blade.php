@extends('layouts.principal')

@section('bienvenida')
    <h1>Usuarios</h1>
    <p>Bienvenidos a Asistance</p>
	<p>Por favor, complete el formulario.</p>
	@include('alertas.alerta')
	@include('alertas.validaformulario')
	<section>
		{!! Form::open(['route' => 'main.usuarios.administrar.store', 'method' => 'POST']) !!}
			<div class="fields">
				<div class="field">
					{!! Form::label('RUT', 'Ingrese su RUT sin puntos, espacios ni guiones. Si el RUT termina en K, escriba un cero en su lugar: ') !!}
					{!! Form::text('RUT', null, ['class' => 'form-control', 'onkeypress' => 'return soloNumeroNatural(event)', 'minlength' => '8', 'maxlength' => '10', 'required oninput' => 'checkRut(this)', 'placeholder' => 'Ejemplo: 123452157']) !!}					
				</div>
				<div class="field half">
					{!! Form::label('Apellido_P', 'Ingrese apellido paterno: ') !!}
					{!! Form::text('Apellido_P', null, ['class' => 'form-control', 'maxlength' => '15', 'placeholder' => 'Ejemplo: González']) !!}
				</div>
				<div class="field half">
					{!! Form::label('Apellido_M', 'Ingrese apellido materno: ') !!}
					{!! Form::text('Apellido_M', null, ['class' => 'form-control', 'maxlength' => '15', 'placeholder' =>'Ejemplo: Pérez']) !!}
				</div>
				<div class="field">
					{!! Form::label('Nombres', 'Ingrese sus nombres: ') !!}
					{!! Form::text('Nombres', null, ['class' => 'form-control', 'maxlength' => '25', 'placeholder' => 'Ejemplo: Juan Carlos']) !!}
				</div>
				<div class="field">
					{!! Form::label('Domicilio', 'Ingrese domicilio: ') !!}
					{!! Form::text('Domicilio', null, ['class' => 'form-control', 'maxlength' => '50', 'placeholder' => 'Ejemplo: Los Tucanes 4567']) !!}
				</div>
				<div class="field half">
					{!! Form::label('Telefono', 'Seleccionar celular asignado: ') !!}
					<select name="Telefono" id="Telefono" class="form-control" required="required">
						@foreach($celulars as $celular)
							<option value="{{ $celular->id_celular}}" id="{{ $celular->id_celular}}" name="{{ $celular->id_celular}}">{{ $celular->Numero_celular }}</option>
						@endforeach
					</select>
				</div>
				<div class="field half">
					{!! Form::label('Fecha_nacimiento', 'Seleccionar fecha nacimiento: ') !!}
					{!! Form::date('Fecha_nacimiento', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
				</div>
				<div class="field half">
					{!! Form::label('Fecha_ingreso', 'Seleccionar fecha ingreso a la compañia: ') !!}
					{!! Form::date('Fecha_ingreso', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
				</div>
				<div class="field quarter">
					{!! Form::label('Rol', 'Seleccionar rol de usuario: ') !!}
					{!!	Form::select('Rol', ['Trabajador'=>'Trabajador','Administrativo'=> 'Administrativo'], 'Trabajador') !!}
				</div>
				<div class="field">
					{!! Form::label('password', 'Ingrese contraseña inicial a asignar: ') !!}
					{!! Form::text('password', 1111, ['class' => 'form-control', 'maxlength' => '30', 'minlength' => '4']) !!}
				</div>
				<div class="field">
					{!! Form::label('email', 'Correo electrónico: ') !!}
					{!! Form::email('email', null, ['class' => 'form-control', 'maxlength' => '40', 'placeholder' => 'Ejemplo: hola@hola.cl']) !!}
				</div>
			</div>
			<ul class="actions">
				<li>
					{!! Form::submit('Generar nuevo usuario',['class' => 'btn btn-primary']) !!}
				</li>
			</ul>
		{!! Form::close() !!}
	</section>
		<p>Todos los campos de este formulario son obligatorios.</p>
@stop         

@section('submenu')
@include('menus.usuarios')
@stop              

@section('subseccion11')
@include('menus.menus_ayuda')
@stop  
