@extends('layouts.principal')

@section('bienvenida')
    <h1>Usuarios</h1>
    <p>Bienvenidos a Asistance</p>
    <p>Administración de usuarios (celular). Efectúe su edición</p>
    @include('alertas.alerta')
	@include('alertas.validaformulario')
    {!! Form::model($celular, ['route' => ['main.usuarios.administrarcel.update', $celular->id_celular], 'method' => 'PUT']) !!}
    <div class="fields">
        <div class="field half">
            {!! Form::label('Numero_celular', 'Ingrese número celular: ') !!}
            {!! Form::text('Numero_celular', null, ['class' => 'form-control', 'onkeypress' => 'return soloNumeroNatural(event)', 'minlength' => '7', 'maxlength' => '11', 'placeholder' =>'Ejemplo: 99872345']) !!}
        </div>
        <div class="field half">
            {!! Form::label('Compania', 'Ingrese compañía (opcional): ') !!}
            {!! Form::text('Compania', null, ['class' => 'form-control', 'maxlength' => '18', 'placeholder' =>'Ejemplo: Entel']) !!}
            </div>
        <div class="field half">
            {!! Form::label('IMEI', 'Ingrese IMEI del equipo (opcional): ') !!}
            {!! Form::text('IMEI', null, ['class' => 'form-control', 'maxlength' => '18','placeholder' =>'Ejemplo: 1111-1111-1111']) !!}
        </div>
    </div>     
    <ul class="actions">
    <li>
    {!! Form::submit('Actualizar',['class' => 'btn btn-primary']) !!}
    </li>
    </ul>
    {!! Form::close() !!}

    {!! Form::open(['route' => ['main.usuarios.administrarcel.destroy', $celular->id_celular], 'method' => 'DELETE']) !!}
        <ul class="actions">
    <li>{!! Form::submit('Eliminar',['class' => 'btn btn-danger']) !!}</li>
        </ul>
    {!! Form::close() !!}
@stop            

@section('submenu')
@include('menus.usuarios')
@stop           

@section('subseccion11')
@include('menus.menus_ayuda')
@stop   