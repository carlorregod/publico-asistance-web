@extends('layouts.principal')

@section('bienvenida')
    <h1>Usuarios</h1>
    <p>Bienvenidos a Asistance</p>
    <p>Administración de usuarios. Efectúe su edición</p>
	@include('alertas.alerta')
	@include('alertas.validaformulario')
    {!! Form::model($usuario, ['route' => ['main.usuarios.administrar.update', $usuario->RUT], 'method' => 'PUT']) !!}
        <div class="fields">
            <div class="field">
                {!! Form::label('RUT', 'RUT de usuario a actualizar: ') !!}
                {!! Form::text('RUT', null, ['readonly'=>'readonly', 'class' => 'form-control']) !!}			
            </div>
            <div class="field half">
                {!! Form::label('Apellido_P', 'Ingrese apellido paterno: ') !!}
                {!! Form::text('Apellido_P', null, ['class' => 'form-control', 'maxlength' => '15', 'placeholder' => 'Ejemplo: González']) !!}
            </div>
            <div class="field half">
                {!! Form::label('Apellido_M', 'Ingrese apellido materno: ') !!}
                {!! Form::text('Apellido_M', null, ['class' => 'form-control', 'maxlength' => '15', 'placeholder' =>'Ejemplo: Pérez']) !!}
            </div>
            <div class="field">
                {!! Form::label('Nombres', 'Ingrese sus nombres: ') !!}
                {!! Form::text('Nombres', null, ['class' => 'form-control', 'maxlength' => '25', 'placeholder' => 'Ejemplo: Juan Carlos']) !!}
            </div>
            <div class="field">
                {!! Form::label('Domicilio', 'Ingrese domicilio: ') !!}
                {!! Form::text('Domicilio', null, ['class' => 'form-control', 'maxlength' => '50', 'placeholder' => 'Ejemplo: Los Tucanes 4567']) !!}
            </div>
            <div class="field half">
            {!! Form::label('Telefono', 'Seleccionar celular asignado: ') !!}
                <select name="Telefono" id="Telefono" class="form-control" required="required">
                    @foreach($celulars as $celular)
                        <option value="{{ $celular->id_celular}}" id="{{ $celular->id_celular}}" name="{{ $celular->id_celular}}">{{ $celular->Numero_celular }}</option>
                    @endforeach
                </select>
            </div>
            <div class="field half">
                {!! Form::label('Fecha_nacimiento', 'Seleccionar fecha nacimiento: ') !!}
                {!! Form::date('Fecha_nacimiento', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
            </div>
            <div class="field half">
                {!! Form::label('Fecha_ingreso', 'Seleccionar fecha ingreso a la compañia: ') !!}
                {!! Form::date('Fecha_ingreso', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control', 'placeholder' =>'Ejemplo: 30-03-2000', 'step'=>'1']) !!}
            </div>
            <div class="field quarter">
                {!! Form::label('Rol', 'Seleccionar rol de usuario: ') !!}
                {!!	Form::select('Rol', ['Trabajador'=>'Trabajador','Administrativo'=> 'Administrativo'], 'Trabajador') !!}
            </div>
            <div class="field">
                {!! Form::label('password', 'Ingrese contraseña inicial a asignar: ') !!}
                {!! Form::text('password', 1111, ['class' => 'form-control', 'maxlength' => '30', 'minlength' => '4']) !!}
            </div>
            <div class="field">
                {!! Form::label('email', 'Correo electrónico: ') !!}
                {!! Form::email('email', null, ['class' => 'form-control', 'maxlength' => '40', 'placeholder' => 'Ejemplo: hola@hola.cl']) !!}
            </div>
        </div>
    <ul class="actions">
    <li>
    {!! Form::submit('Actualizar',['class' => 'btn btn-primary']) !!}
    </li>
    </ul>
    {!! Form::close() !!}

    {!! Form::open(['route' => ['main.usuarios.administrar.destroy', $usuario->RUT], 'method' => 'DELETE']) !!}
        <ul class="actions">
    <li>{!! Form::submit('Eliminar',['class' => 'btn btn-danger']) !!}</li>
        </ul>
    {!! Form::close() !!}
@stop            

@section('submenu')
@include('menus.usuarios')
@stop           

@section('subseccion11')
@include('menus.menus_ayuda')
@stop   