/*Declaración de variables globales: */
//variable del marcador
var marker;  
var marker1;
//coordenadas obtenidas con la geolocalización        
var coords = {}; 

//Funcion principal
initMap = function () 
{
    //usamos la API para geolocalizar el usuario
        navigator.geolocation.getCurrentPosition(
          function (position){
            coords =  {
              lng: position.coords.longitude,
              lat: position.coords.latitude
            };
            setMapa(coords);  //pasamos las coordenadas al metodo para crear el mapa
          },function(error){console.log(error);});    
}

function setMapa (coords)
{   
      //Se crea una nueva instancia del objeto mapa
      var map = new google.maps.Map(document.getElementById('map'),
      {
        zoom: 13,
        center:new google.maps.LatLng(coords.lat,coords.lng),

      });
    //Creamos el marcador 1 en el mapa con sus propiedades
     //para nuestro obetivo tenemos que poner el atributo draggable en true
     //position pondremos las mismas coordenas que obtuvimos en la geolocalización
      marker1 = new google.maps.Marker({
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP,
        position: new google.maps.LatLng(coords.lat,coords.lng),
        icon: {
            path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
            scale: 7, //tamaño
            strokeColor: '#f00', //color del borde
            strokeWeight: 1, //grosor del borde
            fillColor: '#FF0000', //color de relleno
            fillOpacity:1// opacidad del relleno
          },
      });
      //Creamos el marcador 1 en el mapa con sus propiedades
      //para nuestro obetivo tenemos que poner el atributo draggable en true
      //position pondremos las mismas coordenas que obtuvimos en la geolocalización
      marker = new google.maps.Marker({
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP,
        position: new google.maps.LatLng(coords.lat+0.000000000001,coords.lng+0.000000000001),
        icon: {
            path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
            scale: 7, //tamaño
            strokeColor: '#f00', //color del borde
            strokeWeight: 1, //grosor del borde
            fillColor: '#00f', //color de relleno
            fillOpacity:1// opacidad del relleno
          },
      });
      //agregamos un evento al marcador junto con la funcion callback al igual que el evento dragend que indica 
      //cuando el usuario a soltado el marcador
      marker.addListener('click', toggleBounce);
      marker1.addListener('click', toggleBounce);
      
      marker.addListener( 'dragend', function (event)
      {
        //escribimos las coordenadas de la posicion actual del marcador dentro del input con id lat1 y long1 del HTML...
        document.getElementById("lat1").value = this.getPosition().lat();
        document.getElementById("long1").value = this.getPosition().lng();
      });
      marker1.addListener( 'dragend', function (event)
      {
        //escribimos las coordenadas de la posicion actual del marcador dentro del input con id lat1 y long1 del HTML...
        document.getElementById("lat2").value = this.getPosition().lat();
        document.getElementById("long2").value = this.getPosition().lng();
      });
      
}

//callback al hacer clic en el marcador lo que hace es quitar y poner la animacion BOUNCE
function toggleBounce() {
  if (marker.getAnimation() !== null) {
    marker.setAnimation(null);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
  }
}
// Carga de la libreria de google maps 

