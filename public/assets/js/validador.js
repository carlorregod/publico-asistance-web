/* Valida en caja de texto solo numeros, admitiendo negativos y decimales: */
function soloNumeroReal(e) {
    var teclaPulsada = window.event ? window.event.keyCode : e.which;
    var valor = document.getElementById("numero").value;
    if (teclaPulsada == 45 && valor.indexOf("-") == -1)
    {
        document.getElementById("numero").value = "-" + valor;
    }
    if (teclaPulsada == 13 || (teclaPulsada == 46 && valor.indexOf(".") == -1))
    {
        return false;
    }
    if (teclaPulsada == 8)
        return true;
    return /\d/.test(String.fromCharCode(teclaPulsada));
};

/* Validador de números enteros positivos solamente */
function soloNumeroNatural(e)
{
        //captura de evento de presionar una tecla, llamada desde el html, en cada textbox
        //debido a que se trabaja con dinero, no se aceptarán números negativos, el php
        //reconocerá los gastos como negativos y el sueldo como positivo, no en el formulario
	tecla = (document.all) ? e.keyCode : e.which; 
        //tecla 8 es la tecla de borrar, el textbox aceptará borrar caracteres
	if (tecla==8) return true; 
	patron =/^[0-9]+$/;//este no acepta punto(.), si se quiere incluir, añadir un punto (.) después del 9. 
	te = String.fromCharCode(tecla); 
	return patron.test(te);
};

//Para el formulario de RUT. Se trabajará con:
function checkRut(rut) {
    // Despejar Puntos
    var valor = rut.value.replace('.','');
    // Despejar Guión
    valor = valor.replace('-','');
    // Aislar Cuerpo y Dígito Verificador
    cuerpo = valor.slice(0,-1);
    dv = valor.slice(-1).toUpperCase(); 
    // Formatear RUN, se dejará comentado porque se está controlando el ingreso de caracteres especiales:
    //rut.value = cuerpo + '-'+ dv
    // Si no cumple con el mínimo ej. (n.nnn.nnn)
    if(cuerpo.length < 7) { rut.setCustomValidity("RUT Incompleto"); return false;}  
    // Calcular Dígito Verificador
    suma = 0;
    multiplo = 2; 
    // Para cada dígito del Cuerpo
    for(i=1;i<=cuerpo.length;i++) 
   {
        // Obtener su Producto con el Múltiplo Correspondiente
        index = multiplo * valor.charAt(cuerpo.length - i); 
        // Sumar al Contador General
        suma = suma + index;
        // Consolidar Múltiplo dentro del rango [2,7]
        if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
    }
    
    // Calcular Dígito Verificador en base al Módulo 11
    dvEsperado = 11 - (suma % 11);
    
    // Casos Especiales (0 y K)
    dv = (dv == 'K')?10:dv;
    dv = (dv == 0)?11:dv;
    
    // Validar que el Cuerpo coincide con su Dígito Verificador
    if(dvEsperado != dv) { rut.setCustomValidity("RUT Inválido"); return false; }
    
    // Si todo sale bien, eliminar errores (decretar que es válido)
    rut.setCustomValidity('');
}
